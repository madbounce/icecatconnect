<?php
namespace ICEShop\ICECatConnect\Model;

use \Magento\Catalog\Api\ProductAttributeManagementInterface;
use Magento\Framework\Webapi\Exception;
use \Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ObjectManager;

/**
 * Defines the implementaiton class of the calculator service contract.
 */
class ICECatConnectCatalogProductAttributeApi implements \Magento\Catalog\Api\ProductAttributeManagementInterface
{
    /**
     * @var \Magento\Eav\Api\AttributeManagementInterface
     */
    public $eavAttributeManagement;

    public $connection;

    public $conversions_types = [];

    public $conversions_rules = [];

    private $resource = null;

    /**
     * @param \Magento\Eav\Api\AttributeManagementInterface $eavAttributeManagement
     */
    public function __construct(
        \Magento\Eav\Api\AttributeManagementInterface $eavAttributeManagement
    ) {
        $this->eavAttributeManagement = $eavAttributeManagement;
    }

    /**
     * {@inheritdoc}
     */
    public function assign($attributeSetId, $attributeGroupId, $attributeCode, $sortOrder)
    {
        return $this->eavAttributeManagement->assign(
            \Magento\Catalog\Api\Data\ProductAttributeInterface::ENTITY_TYPE_CODE,
            $attributeSetId,
            $attributeGroupId,
            $attributeCode,
            $sortOrder
        );
    }

    /**
     * {@inheritdoc}
     */
    public function unassign($attributeSetId, $attributeCode)
    {
        return $this->eavAttributeManagement->unassign($attributeSetId, $attributeCode);
    }

    private function _getResource()
    {
        if (!$this->resource) {
            $this->resource = ObjectManager::getInstance()->create('\Magento\Framework\App\ResourceConnection');
        }
        return $this->resource;
    }

    /**
     * {@inheritdoc}
     */
    public function getAttributes($attributeSetId)
    {
        $this->_getConnection();
        $this->_getResource();

        $eav_entity_attribute_table = $this->resource->getTableName('eav_entity_attribute');
        $eav_attribute_table = $this->resource->getTableName('eav_attribute');
        $sql = "SELECT eet.*,ea.* FROM " . $eav_entity_attribute_table . " eet LEFT JOIN " . $eav_attribute_table ." ea 
        ON eet.attribute_id = ea.attribute_id 
        WHERE eet.entity_type_id = " . ObjectManager::getInstance()->create(
            '\Magento\Catalog\Model\Product'
        )->getResource()->getEntityType()->getId() . " 
        AND eet.attribute_set_id = {$attributeSetId};";
        $query = $this->connection->query($sql);
        $result_query = [];
        while ($row = $query->fetch()) {
            $result_query[] = $row;
        }

        $eav_attribute = ObjectManager::getInstance()->create('Magento\Catalog\Model\ResourceModel\Eav\Attribute');
        $result = [];
        foreach ($result_query as $attribute) {
            $attributeResource = $eav_attribute->load($attribute['attribute_id']);
            if ($attributeResource->isInSet($attributeSetId)) {
                if ($attributeResource->isScopeGlobal()
                ) {
                    $scope = 'global';
                } elseif ($attributeResource->isScopeWebsite()) {
                    $scope = 'website';
                } elseif ($attributeResource->isScopeStore()) {
                    $scope = 'store';
                } else {
                    $scope = 'store';
                }

                // set options
                $options = [];
                if ($attributeResource->usesSource()) {
                    $attribute_ = ObjectManager::getInstance()->get('\Magento\Eav\Model\Config')->getAttribute(
                        'catalog_product',
                        $attribute['attribute_code']
                    );
                    $options_ = $attribute_->getSource()->getAllOptions();
                    foreach ($options_ as $opt) {
                        $options[] = [
                            'value' => $opt['value'],
                            'label' => (is_object($opt['label'])) ? $opt['label']->getText() : $opt['label'],
                            'external_id' => $this->_getConversionRule($opt['value'], 'attribute_option')
                        ];
                    }
                }

                $result[] = [
                    'attribute_id' => $attribute['attribute_id'],
                    'code' => $attribute['attribute_code'],
                    'type' => $attribute['frontend_input'],
                    'required' => $attribute['is_required'],
                    'scope' => $scope,
                    'options' => $options,
                    'external_id' => $this->_getConversionRule($attribute['attribute_id'], 'attribute')
                ];
            }
        }
        return $result;
    }

    /**
     * @return mixed
     */

    public function _getConversionsTypes()
    {
        $this->_getConnection();

        $results = $this->connection->query("SELECT * FROM icecat_imports_conversions");
        while ($row = $results->fetch()) {
            $this->conversions_types[$row['imports_conversions_type']] = $row['imports_conversions_id'];
        }
        return $this->conversions_types;
    }

    public function _deleteAttributeConversions($attribute_id)
    {
        $this->_getConnection();

        if (empty($this->conversions_types)) {
            $this->_getConversionsTypes();
        }

        $this->connection->query(
            'DELETE FROM icecat_imports_conversions_rules
                    WHERE imports_conversions_rules_original = :imports_conversions_rules_original
                    AND imports_conversions_id = :imports_conversions_id',
            [
                ':imports_conversions_rules_original' => $attribute_id,
                ':imports_conversions_id' => $this->conversions_types['attribute']
            ]
        );
    }

    public function _saveConversions($symbol, $original_id, $type)
    {
        $this->_getConnection();
        if (empty($this->conversions_types)) {
            $this->_getConversionsTypes();
        }

        $searchResult = [];
        $search = $this->connection->query(
            "SELECT * FROM icecat_imports_conversions_rules 
                                        WHERE imports_conversions_id = :imports_conversions_id 
                                        AND imports_conversions_rules_symbol = :imports_conversions_rules_symbol 
                                        AND imports_conversions_rules_original = :imports_conversions_rules_original;",
            [':imports_conversions_id' => $this->conversions_types[$type],
                ':imports_conversions_rules_original' => $original_id,
                ':imports_conversions_rules_symbol' => $symbol]
        );

        while ($row = $search->fetch()) {
            $searchResult[] = $row;
        }

        if (empty($searchResult)) {
            $this->connection->query(
                'INSERT IGNORE INTO icecat_imports_conversions_rules (
imports_conversions_id, 
imports_conversions_rules_symbol, 
imports_conversions_rules_original
) VALUES (:imports_conversions_id, :imports_conversions_rules_symbol, :imports_conversions_rules_original)',
                [':imports_conversions_id' => $this->conversions_types[$type],
                    ':imports_conversions_rules_original' => $original_id,
                    ':imports_conversions_rules_symbol' => $symbol]
            );
        }
    }

    public function saveConversions($symbol, $original_id, $type)
    {
        $this->_saveConversions($symbol, $original_id, $type);
    }

    public function _getConversionsRules($type)
    {
        $this->_getConnection();
        if (empty($this->conversions_types)) {
            $this->_getConversionsTypes();
        }

        $results = $this->connection->query(
            "SELECT * FROM icecat_imports_conversions_rules WHERE imports_conversions_id = :imports_conversions_id",
            [':imports_conversions_id' => $this->conversions_types[$type]]
        );
        while ($row = $results->fetch()) {
            $this->conversions_types[$type][$row['imports_conversions_rules_original']] =
                $row['imports_conversions_rules_symbol'];
        }
        return $this->conversions_rules;
    }

    public function _getConversionRule($original_id, $type)
    {
        if (!empty($type)) {
            if (!array_key_exists($type, $this->conversions_rules)) {
                $this->_getConversionsRules($type);
            }
            if (isset($original_id) && is_scalar($original_id)) {
                if (array_key_exists($type, $this->conversions_rules)
                    && array_key_exists($original_id, $this->conversions_rules[$type])
                ) {
                    return $this->conversions_rules[$type][$original_id];
                }
            }
        }

        return false;
    }

    public function _getConnection()
    {
        if (!$this->connection) {
            $resource = ObjectManager::getInstance()->create('\Magento\Framework\App\ResourceConnection');
            $this->connection = $resource->getConnection(
                \Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION
            );
        }
        return $this->connection;
    }

    /**
     * Create new product attribute
     *
     * @param array $data input data
     * @return integer
     */
    public function create($data)
    {

        $model = ObjectManager::getInstance()->create('\Magento\Eav\Model\Entity\Attribute');
        $helper = ObjectManager::getInstance()->create('\Magento\Catalog\Helper\Product');

        if (empty($data['attribute_code']) || (isset($data['frontend_label']) && !is_array($data['frontend_label']))) {
            $this->_fault('invalid_parameters');
        }

        //validate attribute_code
        if (!preg_match('/^[a-z][a-z_0-9]{0,254}$/', $data['attribute_code'])) {
            $this->_fault('invalid_code');
        }

        //validate frontend_input
        $allowedTypes = [];
        foreach ($this->types() as $type) {
            $allowedTypes[] = $type['value'];
        }
        if (!in_array($data['frontend_input'], $allowedTypes)) {
            $this->_fault('invalid_frontend_input');
        }

        //try to search attribute
            $search_attribute = ObjectManager::getInstance()->create('\Magento\Eav\Model\Entity\Attribute');
            $findAttribute = $search_attribute->loadByCode(ObjectManager::getInstance()->create(
                'Magento\Eav\Model\Entity\Type'
            )->loadByCode('catalog_product')->getId(), $data['attribute_code']);
            if (!empty($findAttribute->getId())) {
                $model = $findAttribute;
            }
        //

            $data['source_model'] = $helper->getAttributeSourceModelByInputType($data['frontend_input']);
            $data['backend_model'] = $helper->getAttributeBackendModelByInputType($data['frontend_input']);
            if ($model->getIsUserDefined() === null || $model->getIsUserDefined() != 0) {
                $data['backend_type'] = $model->getBackendTypeByInput($data['frontend_input']);
            }
    
            $this->_prepareDataForSave($data);
    
            $model->addData($data);
            $model->setEntityTypeId(ObjectManager::getInstance()->create(
                '\Magento\Catalog\Model\Product'
            )->getResource()->getEntityType()->getId());
            $model->setIsUserDefined(1);

            try {
                $model->save();
                $attribute_id = (int)$model->getId();
                if (!empty($data['external_id'])) {
                    $this->_saveConversions($data['external_id'], $attribute_id, 'attribute');
                }
            } catch (Exception $e) {
                $this->_fault('unable_to_save', $e->getMessage());
            }
            return (int)$model->getId();
    }

    /**
     * Retrieve list of possible attribute types
     *
     * @return array
     */
    public function types()
    {
        $types = ObjectManager::getInstance()->create(
            '\Magento\Catalog\Model\Product\Attribute\Source\Inputtype'
        )->toOptionArray();
        return $types;
    }

    public function prepareDataForSave(&$data)
    {
        $this->_prepareDataForSave($data);
    }

    /**
     * Prepare request input data for saving
     *
     * @param array $data input data
     * @return void
     */
    public function _prepareDataForSave(&$data)
    {
        if ($data['scope'] == 'global') {
            $data['is_global'] = \Magento\Catalog\Api\Data\EavAttributeInterface::SCOPE_GLOBAL_TEXT;
        } elseif ($data['scope'] == 'website') {
            $data['is_global'] = \Magento\Catalog\Api\Data\EavAttributeInterface::SCOPE_WEBSITE_TEXT;
        } else {
            $data['is_global'] = \Magento\Catalog\Api\Data\EavAttributeInterface::SCOPE_STORE_TEXT;
        }
        if (!isset($data['is_configurable'])) {
            $data['is_configurable'] = 0;
        }
        if (!isset($data['is_filterable'])) {
            $data['is_filterable'] = 0;
        }
        if (!isset($data['is_filterable_in_search'])) {
            $data['is_filterable_in_search'] = 0;
        }
        if (!isset($data['apply_to'])) {
            $data['apply_to'] = '';
        } else {
            if (is_array($data['apply_to'])) {
                $data['apply_to'] = implode(',', $data['apply_to']);
            }
        }
        // set frontend labels array with store_id as keys
        if (isset($data['frontend_label']) && is_array($data['frontend_label'])) {
            $labels = [];
            foreach ($data['frontend_label'] as $label) {
                $storeId = $label['store_id'];
                $labelText = strip_tags($label['label']);
                if (trim($labelText) === '') {
                    $labelText = __('Attribute label was not defined');
                }
                $labels[$storeId] = $labelText;
            }
            $data['frontend_label'] = $labels;
            if (!empty($data['frontend_label'])) {
                foreach ($data['frontend_label'] as $lbl) {
                    $data['frontend_label'][0] = $lbl;
                    break;
                }
            }
        }
        // set additional fields
        if (isset($data['additional_fields']) && is_array($data['additional_fields'])) {
            $data = array_merge($data, $data['additional_fields']);
            unset($data['additional_fields']);
        }
        //default value
        if (!empty($data['default_value'])) {
            $data['default_value'] = strip_tags($data['default_value']);
        }
    }

    public function saveOptionsForAttribute($data, $id)
    {
        if (isset($data['options_'])) {
            $attribute_entity = ObjectManager::getInstance()->create('\Magento\Eav\Model\Entity\Attribute');
            if (!empty($data['options_'])) {
                foreach ($data['options_'] as $key => $value) {
                    if (isset($value['value'])) {
                        $attribute_entity->load($id);
                        $attribute_entity->addData(['option' => $data['options_'][$key]]);
                        $attribute_entity->save();
                    }
                }
            }
        }
    }

    /**
     * Add attribute to attribute set
     *
     * @param string $attributeId
     * @param string $attributeSetId
     * @param string|null $attributeGroupId
     * @param string $sortOrder
     * @return bool
     */
    public function attributeAdd($attributeId, $attributeSetId, $attributeGroupId = null, $sortOrder = '0')
    {
        $attribute_entity = ObjectManager::getInstance()->create('\Magento\Eav\Model\Entity\Attribute');
        // check if attribute with requested id exists
        $attribute = $attribute_entity->load($attributeId);
        if (!$attribute->getId()) {
            $this->_fault('invalid_attribute_id');
        }
        // check if attribute set with requested id exists
        $attributeSet = ObjectManager::getInstance()->create('Magento\Eav\Model\Entity\Attribute\Set')
            ->load($attributeSetId);
        if (!$attributeSet->getId()) {
            $this->_fault('invalid_attribute_set_id');
        }
        if (!empty($attributeGroupId)) {
            // check if attribute group with requested id exists
            $attributeGroup = ObjectManager::getInstance()->create('Magento\Eav\Model\Entity\Attribute\Group');
            if (!$attributeGroup->load($attributeGroupId)->getId()) {
                $this->_fault('invalid_attribute_group_id');
            }
        } else {
            // define default attribute group id for current attribute set
            $attributeGroupId = $attributeSet->getDefaultGroupId();
        }

        try {
            $attribute->setEntityTypeId($attributeSet->getEntityTypeId())
                ->setAttributeSetId($attributeSetId)
                ->setAttributeGroupId($attributeGroupId)
                ->setSortOrder($sortOrder)
                ->save();
        } catch (Exception $e) {
            $this->_fault('add_attribute_error', $e->getMessage());
        }
        return true;
    }

    private function _fault($phrase, $msg = null)
    {
        if (isset($msg)) {
            $phrase = $phrase . '(' . $msg . ')';
        }
        throw new \Exception($phrase);
    }

    public function addOption($attribute, $data)
    {

        $model = ObjectManager::getInstance()->create('\Magento\Eav\Model\Entity\Attribute')->load($attribute);

        if (!$model->usesSource()) {
            $this->_fault('invalid_frontend_input');
        }

        $optionLabels = [];
        foreach ($data['label'] as $label) {
            $storeId = $label['store_id'];
            $labelText = strip_tags($label['value']);
            if (is_array($storeId)) {
                foreach ($storeId as $multiStoreId) {
                    $optionLabels[$multiStoreId] = $labelText;
                }
            } else {
                $optionLabels[$storeId] = $labelText;
            }
        }
        $modelData = [
            'value' => $optionLabels,
            'sort_order' => (int)$data['order'],
            'attribute_id' => (int)$model->getId(),
            'external_id' => $data['external_id']
        ];
        return $this->_saveOption($modelData);
    }
    public function updateOption($attribute, $option, $data)
    {

        $model = ObjectManager::getInstance()->create('\Magento\Eav\Model\Entity\Attribute')->load($attribute);

        if (!$model->usesSource()) {
            $this->_fault('invalid_frontend_input');
        }

        $optionLabels = [];
        foreach ($data['label'] as $label) {
            $storeId = $label['store_id'];
            $labelText = strip_tags($label['value']);
            if (is_array($storeId)) {
                foreach ($storeId as $multiStoreId) {
                    $optionLabels[$multiStoreId] = $labelText;
                }
            } else {
                $optionLabels[$storeId] = $labelText;
            }
        }
        $modelData = [
            'sort_order' => (int)$data['order'],
            'attribute_id' => (int)$model->getId(),
            'external_id' => $data['external_id']
        ];

        $scopeConfig = ObjectManager::getInstance()->get('\Magento\Framework\App\Config\ScopeConfigInterface');
        if ($scopeConfig->getValue('iceshop_icecatconnect/icecatconnect_service_settings/products_update_attributes')) {
            $modelData['value'] = $optionLabels;
        }

        return $this->_saveOption($modelData, $option);
    }

    public function _saveOption($option, $optionId = null)
    {

        $this->_getConnection();

        if (is_array($option)) {
            $resource = ObjectManager::getInstance()->create('\Magento\Framework\App\ResourceConnection');
            $adapter = $resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
            $optionTable = $resource->getTableName('eav_attribute_option');
            $optionValueTable = $resource->getTableName('eav_attribute_option_value');

            $m_store = ObjectManager::getInstance()->create('Magento\Store\Model\StoreManagerInterface');
            $stores = $m_store->getStores(true);

            if (isset($option['value'])) {
                $values = $option['value'];
                $data = [
                    'attribute_id' => $option['attribute_id'],
                    'sort_order' => 0
                ];

                //rewriting default sort order if received
                if (isset($option['sort_order'])) {
                    $data['sort_order'] = (int)$option['sort_order'];
                }

                if (!$optionId) {
                    $adapter->insert($optionTable, $data);
                    $intOptionId = $adapter->lastInsertId();
                } else {
                    $intOptionId = $optionId;
                }

                if (!empty($option['external_id'])) {
                    $this->_saveConversions($option['external_id'], $intOptionId, 'attribute_option');
                }
                // Default value
                if (!isset($values[0])) {
                    if (!empty($values)) {
                        foreach ($values as $lbl) {
                            $values[0] = $lbl;
                            break;
                        }
                    }
                }

                foreach ($stores as $store) {
                    if (isset($values[$store->getId()])
                        && (!empty($values[$store->getId()])
                            || $values[$store->getId()] == "0")
                    ) {
                        $data = [
                            'option_id' => $intOptionId,
                            'store_id' => $store->getId(),
                            'value' => $values[$store->getId()],
                        ];
                        if ($optionId) {
                            $where = $adapter->quoteInto('option_id =?', $optionId);
                            $adapter->update($optionValueTable, $data, $where);
                        } else {
                            $adapter->insert($optionValueTable, $data);
                        }
                    }
                }

                return $intOptionId;
            }
        }

        return false;
    }

    /**
     * Update product data
     *
     * @param $productId
     * @param $productData
     * @param null $store
     * @param null $identifierType
     * @return bool
     */
    public function update($productId, $productData, $store = null)
    {
        $this->_getConnection();
        $modelProductApi = ObjectManager::getInstance()->create('\Magento\Catalog\Model\Product');

        $product = $modelProductApi->load($productId);
        $product->setStoreId($store);
        $visibility = $product->getVisibility();

        $this->_prepareProductDataForSave($product, $productData);

        try {
            if (is_array($errors = $product->validate())) {
                $strErrors = [];
                foreach ($errors as $code => $error) {
                    if ($error === true) {
                        $error = __('Value for "%1" is invalid.', $code)->getText();
                    } else {
                        $error = __('Value for "%1" is invalid: %2', $code, $error)->getText();
                    }
                    $strErrors[] = $error;
                }
                $this->_fault('message_data_invalid', implode("\n", $strErrors));
            }
            //fix for Magento2 because they have a bug with this
            $product->setUrlKey($product->formatUrlKey($product->getName(). '-' .$product->getStore()->getId().time()));
            $product->setStatus(\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
            $urlPath = $product->getUrlPath();
            if ($urlPath === false) {
                $product->setUrlPath(null);
            }
            if (!empty($visibility)) {
                $product->setVisibility($visibility);
            } else {
                $product->setVisibility(\Magento\Catalog\Model\Product\Visibility::VISIBILITY_NOT_VISIBLE);
            }

            $product->save();
        } catch (\Exception $e) {
            $this->_fault('data_invalid', $e->getMessage());
        }

        return true;
    }

    /**
     *  Set additional data before product saved
     */
    public function _prepareProductDataForSave($product, $productData)
    {
        if (isset($productData['website_ids']) && is_array($productData['website_ids'])) {
            $product->setWebsiteIds($productData['website_ids']);
        }

        if (!empty($productData['attribute_set'])) {
            if ($product->getAttributeSetId() != $productData['attribute_set']) {
                //attribute set will be changed, need to drop all the attributes values (added by the Iceshop)
//                $helper = Mage::helper('icecatconnector');
                try {
                    $this->cleanupProduct($product->getId());
                } catch (Exception $e) {
                    // Exception $e
                }
            }
            $product->setAttributeSetId($productData['attribute_set']);
        }

        foreach ($product->getTypeInstance(true)->getEditableAttributes($product) as $attribute) {
            //Unset data if object attribute has no value in current store
            if (\Magento\Store\Model\Store::DEFAULT_STORE_ID !== (int)$product->getStoreId()
                && !$product->getExistsStoreValueFlag($attribute->getAttributeCode())
                && !$attribute->isScopeGlobal()
            ) {
                $product->setData($attribute->getAttributeCode(), false);
            }
            if (isset($productData[$attribute->getAttributeCode()])) {
                    $product->setData(
                        $attribute->getAttributeCode(),
                        $productData[$attribute->getAttributeCode()]
                    );
            } elseif (isset($productData['additional_attributes']['single_data'][$attribute->getAttributeCode()])) {
                    $product->setData(
                        $attribute->getAttributeCode(),
                        $productData['additional_attributes']['single_data'][$attribute->getAttributeCode()]
                    );
            } elseif (isset($productData['additional_attributes']['multi_data'][$attribute->getAttributeCode()])) {
                    $product->setData(
                        $attribute->getAttributeCode(),
                        $productData['additional_attributes']['multi_data'][$attribute->getAttributeCode()]
                    );
                    // If attribute value has been removed but wasn't transmitted
            } elseif (preg_match('/^i_.*_[0,1]$/', $attribute->getAttributeCode())) {
                    $product->setData($attribute->getAttributeCode(), false);
            }
        }

        if (isset($productData['categories']) && is_array($productData['categories'])) {
            $product->setCategoryIds($productData['categories']);
        }
        $websiteInterface = ObjectManager::getInstance()->create('\Magento\Store\Model\StoreManagerInterface');
        if (isset($productData['websites']) && is_array($productData['websites'])) {
            foreach ($productData['websites'] as &$website) {
                if (is_string($website)) {
                    try {
                        $website = $websiteInterface->getWebsite($website)->getId();
                    } catch (Exception $e) {
                    }
                }
            }
            $product->setWebsiteIds($productData['websites']);
        }

        $storeManager = ObjectManager::getInstance()->get('\Magento\Store\Model\StoreManagerInterface');

        if ($websiteInterface->isSingleStoreMode()) {
            $storeManager = ObjectManager::getInstance()->get('\Magento\Store\Model\StoreManagerInterface');
            $product->setWebsiteIds([$storeManager->getStore()->getWebsiteId()]);
        }

        if (isset($productData['stock_data']) && is_array($productData['stock_data'])) {
            $product->setStockData($productData['stock_data']);
        }

        if (isset($productData['tier_price']) && is_array($productData['tier_price'])) {
            $product->setData(
                \Magento\Catalog\Api\Data\ProductAttributeInterface::CODE_TIER_PRICE,
                $productData['tier_price']
            );
        }
    }

    public function cleanupProduct($product_id, $trash_cleanup = false)
    {
        $this->_getConnection();
        if (!empty($product_id)) {
            $product_set_id = ObjectManager::getInstance()->create(
                '\Magento\Catalog\Model\Product'
            )->load($product_id)->getAttributeSetId();
            $resource = ObjectManager::getInstance()->create('\Magento\Framework\App\ResourceConnection');
            $read_resource = $this->connection;
            $write_resource = $this->connection;

            $comparison_operation = '=';
            if ($trash_cleanup == true) {
                $comparison_operation = '<>';
            }
            $select = $this->connection->query('SELECT
            t1.attribute_id
            FROM ' . $resource->getTableName('eav_attribute') . ' AS t1
            JOIN icecat_imports_conversions_rules AS t2
            ON t1.attribute_code = t2.imports_conversions_rules_symbol
            JOIN icecat_imports_conversions AS t3
            ON t2.imports_conversions_id = t3.imports_conversions_id
            JOIN ' . $resource->getTableName('eav_entity_attribute') . ' AS t4
            ON t1.attribute_id = t4.attribute_id
            WHERE t3.imports_conversions_type = \'attribute\'
            AND t4.attribute_set_id ' . $comparison_operation . ' ' . $product_set_id . ';');

            try {
                //fetching all the iceshop attributes IDs
                $iceshop_attributes_id = [];
                while ($row = $select->fetch()) {
                    $iceshop_attributes_id[] = $row;
                }

                if (!empty($iceshop_attributes_id)) {
                    //formatting attributes in a string to use in IN stmt
                    $attributes_ids_str = '';
                    $divider = '';
                    foreach ($iceshop_attributes_id as $attribute_id) {
                        $attributes_ids_str .= $divider . $attribute_id['attribute_id'];
                        $divider = ',';
                    }

                    //deleting values from varchar table
                    $query = 'DELETE FROM ' . $resource->getTableName('catalog_product_entity_varchar') . '
                    WHERE entity_id = ' . $product_id . ' AND attribute_id IN (' . $attributes_ids_str . ');';
                    $write_resource->query($query);
                    //deleting values from int table
                    $query = 'DELETE FROM ' . $resource->getTableName('catalog_product_entity_int') . '
                    WHERE entity_id = ' . $product_id . ' AND attribute_id IN (' . $attributes_ids_str . ');';
                    $write_resource->query($query);
                }
            } catch (Exception $e) {
                $this->_fault('error_during_cleanup `' . $e->getMessage() . '`');
            }

            return true;
        }
        return false;
    }
}
