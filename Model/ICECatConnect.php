<?php

namespace ICEShop\ICECatConnect\Model;

use ICEShop\ICECatConnect\Api\ICECatConnectInterface;
use Magento\Framework\App\Action\Context;
use \Magento\Catalog\Api\Data\ProductAttributeInterface;
use \Magento\Config\Model\ResourceModel\Config;
use \Magento\Framework\App\ObjectManager;

/**
 * Defines the implementaiton class of the calculator service contract.
 */
class ICECatConnect implements ICECatConnectInterface
{
    /*
     * Constant for set mode `Scheduled` at indexer processes
     */
    const INDEXER_MODE_SCHEDULED = 'scheduled';

    /*
     * Constant for set mode `Update On Save` at indexer processes
     */
    const INDEXER_MODE_UPDATE_ON_SAVE = 'update_on_save';

    public $arFiltersMap = [
        'product_id' => 'entity_id',
        'set' => 'attribute_set_id',
        'type' => 'type_id'
    ];
	//2.0.0-stable

    /*
     * Connection variable
     */
    public $connection = null;

    //git
    /*
     * Resource variable
     */
    public $resource = null;

    public $conversions_rules = null;

    public $conversions_types = null;

    /*
     * Writer to `core_config_data` table
     */
    public $configWriter = null;

    /**
     * Get version of Magento shop
     * @return string
     */

    public function getMessage()
    {
        return 'Hello Magento 2! We will change the world!';
    }

    public function getICEshopIcecatconnectorExtensionVersion()
    {
        $this->getConfigWriter();
        $this->configWriter->save('icecatconnect_content_last_start', time());
        $productMetadata = ObjectManager::getInstance()->get('Magento\Framework\App\ProductMetadataInterface');
        $version = $productMetadata->getVersion();
        return json_encode((string)$version);
    }

    /**
     * Create _configWriter for save to `core_config_data`
     * @return null
     */
    public function getConfigWriter()
    {
        if (!$this->configWriter) {
            $this->configWriter = ObjectManager::getInstance()->create(
                'Magento\Framework\App\Config\Storage\WriterInterface'
            );
        }
        return $this->configWriter;
    }

    /**
     * Get products from shop
     * @param mixed $data
     * @return string
     */
    public function getProductsBatch($data)
    {
        $this->_dataDecode($data);
        $this->_getConnection();
        $this->_getResource();
        $storeId = 0;
        $result = [];
        $page = null;
        $page_size = null;
        if (!empty($data) && is_array($data)) {
            $store = (!empty($data['store'])) ? $data['store'] : null;
            $filters = (!empty($data['filters'])) ? $data['filters'] : null;
            $page = (!empty($data['page'])) ? $data['page'] : null;
            $page_size = (!empty($data['page_size'])) ? $data['page_size'] : null;
        }
        $collection = ObjectManager::getInstance()->create('Magento\Catalog\Model\ResourceModel\Product\Collection')
            ->addStoreFilter($storeId)
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('updated_ice');

        $optionId = false;
        $eav = ObjectManager::getInstance()->get('\Magento\Eav\Model\Config');
        $attribute = $eav->getAttribute('catalog_product', 'active_ice')->getData();
        if (isset($attribute['attribute_id'])) {
            $options = $eav->getAttribute('catalog_product', 'active_ice')->getSource()->getAllOptions();
            foreach ($options as $option) {
                if ($option['label'] == 'Yes') {
                    $optionId = $option['value'];
                    break;
                }
            }
        }

        if ($optionId !== false) {
            $sql = "UPDATE {$this->resource->getTableName('catalog_product_entity')} 
SET active_ice = " . $optionId . "  WHERE active_ice IS NULL; ";
            $exec = $this->connection->query($sql);
            $collection->addFieldToFilter('active_ice', $optionId);
        }

        $scopeConfig = ObjectManager::getInstance()->get('\Magento\Framework\App\Config\ScopeConfigInterface');
        $mpn_attribute_code = $scopeConfig->getValue(
            'iceshop_icecatconnect/icecatconnect_products_mapping/products_mapping_mpn'
        );
        if (!empty($mpn_attribute_code)) {
            $collection->addAttributeToSelect($mpn_attribute_code);
        }
        $brand_name_attribute_code = $scopeConfig->getValue(
            'iceshop_icecatconnect/icecatconnect_products_mapping/products_mapping_brand'
        );
        if (!empty($brand_name_attribute_code)) {
            $collection->addAttributeToSelect($brand_name_attribute_code);
        }
        $ean_attribute_code = $scopeConfig->getValue(
            'iceshop_icecatconnect/icecatconnect_products_mapping/products_mapping_gtin'
        );
        if (!empty($ean_attribute_code)) {
            $collection->addAttributeToSelect($ean_attribute_code);
        }
        if (!empty($filters)) {
            $filters = $this->parseFilters($filters, $this->filtersMap());
            try {
                foreach ($filters as $field => $value) {
                    $collection->addFieldToFilter($field, $value);
                }
            } catch (\Exception $e) {
                $result['API_ERROR'] = [
                    'comment' => 'Filters invalid',
                ];
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $result['API_ERROR'] = [
                    'comment' => 'Filters invalid',
                ];
            }
        }

        $brand_name_attribute_type = false;
        if ($page !== null && $page_size !== null) {
            $collection = $collection
                ->setPageSize($page_size)
                ->setCurPage($page);
        }
        $total_products = $collection->getSize();
        if ($total_products >= ($page_size * ($page - 1))) {
            foreach ($collection as $product) {
                $item = [
                    'product_id' => $product->getId(),
                    'updated' => $product->getData('updated_ice')
                ];

                if (!empty($mpn_attribute_code)) {
                    $item['mpn'] = $product->getData($mpn_attribute_code);
                }
                if (!empty($brand_name_attribute_code)) {
                    if ($brand_name_attribute_type == false) {
                        $brand_name_attribute_type = $product->getResource()
                            ->getAttribute($brand_name_attribute_code)
                            ->getFrontend()
                            ->getInputType();
                    }
                    switch ($brand_name_attribute_type) {
                        case 'select':
                        case 'dropdown':
                            $item['brand_name'] = $product->getAttributeText($brand_name_attribute_code);
                            break;
                        case 'text':
                            $item['brand_name'] = $product->getData($brand_name_attribute_code);
                            break;
                    }
                }
                if (!empty($ean_attribute_code)) {
                    $item['ean'] = $product->getData($ean_attribute_code);
                }
                $result['products'][] = $item;
            }
        }
        $defaultSetId = ObjectManager::getInstance()->create('Magento\Catalog\Model\Product')
            ->getDefaultAttributeSetid();
        $result['default_attribute_set'] = $defaultSetId;
        return json_encode($result);
    }

    /**
     * Get language mapping
     * @return string
     */
    public function getLanguageMapping()
    {
        $scopeConfig = ObjectManager::getInstance()->get('\Magento\Framework\App\Config\ScopeConfigInterface');
        $is_multilingual = $scopeConfig->getValue(
            'iceshop_icecatconnect/icecatconnect_language_mapping/multilingual_mode'
        );
        $attribute_update_required = $scopeConfig->getValue(
            'iceshop_icecatconnect/icecatconnect_service_settings/products_update_attributes'
        );

        $attribute_sort_order_update_required = 0;
        $attribute_labels_update_required = 0;
        if (!isset($attribute_update_required)) {
            $attribute_update_required = 1;
        }
        if ($attribute_update_required == 1) {
            //fetch label/sort_order settings
            $attribute_sort_order_update_required = $scopeConfig->getValue(
                'iceshop_icecatconnect/icecatconnect_service_settings/products_update_sort_order'
            );
            $attribute_labels_update_required = $scopeConfig->getValue(
                'iceshop_icecatconnect/icecatconnect_service_settings/update_attribute_labels'
            );
        }
        if ($is_multilingual == 1) {
            //multilingual mode
            $mapping = $scopeConfig->getValue(
                'iceshop_icecatconnect/icecatconnect_language_mapping/multilingual_values'
            );

            //fix store_id = 0
            $mapping = json_decode($mapping, true);
            if (isset($mapping[$this->_getStoreId()])) {
                $mapping[0] = $mapping[$this->_getStoreId()];
                $mapping[0]['store_id'] = 0;
                $mapping = json_encode($mapping);
            }
        } else {
            //one language mode
            $mapping = [
                (object)[
                    'store_id' => 0,
                    'value' => $scopeConfig->getValue(
                        'iceshop_icecatconnect/icecatconnect_language_mapping/main_language_single'
                    )
                ],
                (object)[
                    'store_id' => $this->_getStoreId(),
                    'value' => $scopeConfig->getValue(
                        'iceshop_icecatconnect/icecatconnect_language_mapping/main_language_single'
                    )
                ],
                (object)[
                    'store_id' => 9999,
                    'value' => $scopeConfig->getValue(
                        'iceshop_icecatconnect/icecatconnect_language_mapping/insurance_language_single'
                    )
                ]
            ];
            $mapping = json_encode($mapping);
        }
        $response = [
            'multi' => $is_multilingual,
            'mapping' => $mapping,
            'exact_language_import' => $scopeConfig->getValue(
                'iceshop_icecatconnect/icecatconnect_language_mapping/strict_language_import'
            ),
            'attribute_update_required' => $attribute_update_required,
            'attribute_sort_order_update_required' => $attribute_sort_order_update_required,
            'attribute_labels_update_required' => $attribute_labels_update_required
        ];

        // Import main mapping attributes if empty values
        $main_attributes_mapping = $this->getICEshopMapping();
        if (($main_attributes_mapping['mpn'] != '') && ($main_attributes_mapping['brand_name'] != '')
            && ($main_attributes_mapping['ean'] != '')) {
            $import_main_attributes = [];
            $import_main_attributes['mapping'] = $main_attributes_mapping;
            $import_main_attributes['ean'] = $scopeConfig->getValue(
                'iceshop_icecatconnect/icecatconnect_products_mapping/import_gtin'
            );
            $import_main_attributes['brand_mpn'] = $scopeConfig->getValue(
                'iceshop_icecatconnect/icecatconnect_products_mapping/import_brand_mpn'
            );
            $response['import_main_attributes'] = $import_main_attributes;
        }
        if (!empty($main_attributes_mapping) && array_key_exists('description', $main_attributes_mapping)) {
            $response['description'] = $main_attributes_mapping['description'];
        }
        return json_encode($response);
    }

    public function getICEshopMapping()
    {
        $scopeConfig = ObjectManager::getInstance()->get('\Magento\Framework\App\Config\ScopeConfigInterface');
        $mapping = [];
        $mapping['mpn'] = $scopeConfig->getValue(
            'iceshop_icecatconnect/icecatconnect_products_mapping/products_mapping_mpn'
        );
        $mapping['brand_name'] = $scopeConfig->getValue(
            'iceshop_icecatconnect/icecatconnect_products_mapping/products_mapping_brand'
        );
        $mapping['ean'] = $scopeConfig->getValue(
            'iceshop_icecatconnect/icecatconnect_products_mapping/products_mapping_gtin'
        );
        if ($scopeConfig->getValue(
            'iceshop_icecatconnect/icecatconnect_products_mapping/default_description_attributes'
        ) == 0) {
            $mapping['description']['name'] = $scopeConfig->getValue(
                'iceshop_icecatconnect/icecatconnect_products_mapping/name_attribute'
            );
            $mapping['description']['short_description'] = $scopeConfig->getValue(
                'iceshop_icecatconnect/icecatconnect_products_mapping/short_description_attribute'
            );
            $mapping['description']['long_description'] = $scopeConfig->getValue(
                'iceshop_icecatconnect/icecatconnect_products_mapping/long_description_attribute'
            );
            $mapping['description']['icecat_products_name'] = $scopeConfig->getValue(
                'iceshop_icecatconnect/icecatconnect_products_mapping/icecat_products_name_attribute'
            );
        }
        return $mapping;
    }

    /**
     * Get attribute sets from Magento shop
     * @param array $data
     * @return string
     */
    public function catalogProductAttributeSetList($data)
    {
        $this->_dataDecode($data);
        $entityType = ObjectManager::getInstance()->get('\Magento\Catalog\Model\Product')->getResource()
            ->getEntityType();
        $collection = ObjectManager::getInstance()->get(
            '\Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\Collection'
        )->setEntityTypeFilter($entityType->getId());
        $collection = $collection->getItems();

        $limited = false;
        if (!empty($data)) {
            if (array_key_exists('start', $data)) {
                $start = (int)$data['start'];
                if ($start < 0) {
                    $start = 0;
                }
                $limited = true;
            }
            if (array_key_exists('limit', $data)) {
                $limit = (int)$data['limit'];
                if ($limit <= 0) {
                    $limit = 50;
                }
                $limited = true;
            }
        }
        if ($limited == true) {
            $collection = array_slice($collection, $start, $limit);
        }
        $result = [];
        foreach ($collection as $attributeSet) {
            $groups = ObjectManager::getInstance()->get(
                '\Magento\Eav\Model\ResourceModel\Entity\Attribute\Group\Collection'
            )
                ->setAttributeSetFilter($attributeSet->getId())
                ->load();

            $groups_arr = [];
            foreach ($groups as $group) {
                $groups_arr[] = [
                    'id' => $group->getId(),
                    'name' => $group->getAttributeGroupName(),
                    'external_id' => $this->_getConversionRule($group->getId(), 'attribute_group')
                ];
            }

            $attributes = ObjectManager::getInstance()->get(
                '\Magento\Eav\Model\ResourceModel\Entity\Attribute\Collection'
            )
                ->setAttributeSetFilter($attributeSet->getId())
                ->load();

            $attributes_arr = [];
            foreach ($attributes as $attribute) {
                if ($conversion = $this->_getConversionRule($attribute->getId(), 'attribute')) {
                    $attributes_arr[] = [
                        'id' => $attribute->getId(),
                        'name' => $attribute->getName(),
                        'external_id' => $conversion
                    ];
                }
            }

            $result[] = [
                'set_id' => $attributeSet->getId(),
                'name' => $attributeSet->getAttributeSetName(),
                'external_id' => $this->_getConversionRule($attributeSet->getId(), 'attribute_set'),
                'groups' => $groups_arr,
                'attributes' => $attributes_arr
            ];
        }
        return json_encode($result);
    }

    /**
     * Get store id
     * @return mixed
     */
    public function _getStoreId()
    {
        $storeManager = ObjectManager::getInstance()->get('\Magento\Store\Model\StoreManagerInterface');
        return $storeManager->getStore()->getId();
    }

    /**
     * Create connection to shop db
     * @return null
     */
    public function _getConnection()
    {
        if (!$this->connection) {
            $resource = ObjectManager::getInstance()->create('\Magento\Framework\App\ResourceConnection');
            $this->connection = $resource->getConnection(
                \Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION
            );
        }
        return $this->connection;
    }

    /**
     * Get conversions types for attributes, sets, attributes groups
     * @return mixed
     */
    public function _getConversionsTypes()
    {
        $this->_getConnection();

        $results = $this->connection->query("SELECT * FROM icecat_imports_conversions");
        while ($row = $results->fetch()) {
            $this->conversions_types[$row['imports_conversions_type']] = $row['imports_conversions_id'];
        }
        return $this->conversions_types;
    }

    /**
     * Save conversions
     *
     * @param $symbol
     * @param $original_id
     * @param $type
     */
    public function _saveConversions($symbol, $original_id, $type)
    {
        $this->_getConnection();
        if (empty($this->conversions_types)) {
            $this->_getConversionsTypes();
        }

        $searchResult = [];
        $search = $this->connection->query(
            "SELECT * FROM icecat_imports_conversions_rules 
                                        WHERE imports_conversions_id = :imports_conversions_id 
                                        AND imports_conversions_rules_symbol = :imports_conversions_rules_symbol 
                                        AND imports_conversions_rules_original = :imports_conversions_rules_original;",
            [':imports_conversions_id' => $this->conversions_types[$type],
                ':imports_conversions_rules_original' => $original_id,
                ':imports_conversions_rules_symbol' => $symbol]
        );

        while ($row = $search->fetch()) {
            $searchResult[] = $row;
        }

        if (empty($searchResult)) {
            $this->connection->query(
                'INSERT IGNORE INTO icecat_imports_conversions_rules (
imports_conversions_id, 
imports_conversions_rules_symbol, 
imports_conversions_rules_original
) VALUES (:imports_conversions_id, :imports_conversions_rules_symbol, :imports_conversions_rules_original)',
                [':imports_conversions_id' => $this->conversions_types[$type],
                    ':imports_conversions_rules_original' => $original_id,
                    ':imports_conversions_rules_symbol' => $symbol]
            );
        }
    }

    public function saveConversions($symbol, $original_id, $type)
    {
        $this->_saveConversions($symbol, $original_id, $type);
    }

    /**
     * @param $type
     * @return mixed
     */
    protected function _getConversionsRules($type)
    {
        $this->_getConnection();
        if (empty($this->conversions_types)) {
            $this->_getConversionsTypes();
        }

        $results = $this->connection->fetchAll(
            "SELECT * FROM icecat_imports_conversions_rules WHERE imports_conversions_id = :imports_conversions_id",
            [':imports_conversions_id' => $this->conversions_types[$type]]
        );
        foreach ($results as $row) {
            $this->conversions_rules[$type][$row['imports_conversions_rules_original']] =
                $row['imports_conversions_rules_symbol'];
        }
        return $this->conversions_rules;
    }

    /**
     * @param $original_id
     * @param $type
     * @return bool
     */
    protected function _getConversionRule($original_id, $type)
    {

        if (empty($this->conversions_rules[$type])) {
            $this->_getConversionsRules($type);
        }
        if (!empty($this->conversions_rules[$type][$original_id])) {
            return $this->conversions_rules[$type][$original_id];
        }

        return false;
    }

    /**
     * @param $data
     * @return string
     */
    public function getProductAttributeList($data)
    {
        $this->_dataDecode($data);
        $response = [];
        if (!empty($data) && is_array($data) && !empty($data['items'])) {
            $attribute_api_model = ObjectManager::getInstance()->create(
                'ICEShop\ICECatConnect\Model\ICECatConnectCatalogProductAttributeApi'
            );

            foreach ($data['items'] as $attribute_set_id) {
                try {
                    $attribute_list = $attribute_api_model->getAttributes($attribute_set_id);
                    $response[$attribute_set_id] = $attribute_list;
                } catch (\Exception $e) {
                    $response['API_ERROR'][$attribute_set_id] = [
                        'comment' => $e->getMessage(),
                        'status' => 'Error while fetching attribute list'
                    ];
                }
            }
        }
        return json_encode($response);
    }

    /**
     * Save sets, attributes, groups for products
     * with data that get from server
     *
     * @param mixed $data
     * @return bool|string
     */
    public function saveAttributeSetBatch($data)
    {
        $this->_getConnection();
        $this->_dataDecode($data);

        if (!empty($data) && is_array($data)) {
            $response = [];
            if (!empty($data['set'])) {
                if (empty($data['set']['magento_id'])) {
                    //create new attribute set
                    try {
                        $attributeSet = ObjectManager::getInstance()->create('Magento\Eav\Model\Entity\Attribute\Set');
                        $entityTypeId = ObjectManager::getInstance()->create('Magento\Eav\Model\Entity\Type')
                            ->loadByCode(
                                'catalog_product'
                            )->getId();
                        $attributeSet->setData([
                            'attribute_set_name' => $data['set'][0],
                            'entity_type_id' => $entityTypeId,
                            'sort_order' => 200,
                        ]);

                        if ($attributeSet->validate()) {
                            $attributeSet->save();
                            $attributeSet->initFromSkeleton($data['set'][1]);
                            $attributeSet->save();
                            $set_id = $attributeSet->getId();
                            if (!empty($data['set'][2]['external_id'])) {
                                $this->_saveConversions($data['set'][2]['external_id'], $set_id, 'attribute_set');
                            }
                            $response['set'] = [
                                'external_id' => $data['set'][2]['external_id'],
                                'comment' => 'Attribute set create successfully',
                                'magento_id' => $set_id
                            ];
                        } else {
                            $response['API_ERROR']['set'] = [
                                'external_id' => $data['set'][2]['external_id'],
                                'comment' => 'Attribute set save validation error',
                                'magento_id' => null
                            ];
                        }
                    } catch (\Exception $e) {
                        $response['API_ERROR']['set'] = [
                            'external_id' => $data['set'][2]['external_id'],
                            'comment' => $e->getMessage(),
                            'magento_id' => null
                        ];
                    } catch (\Magento\Framework\Exception\LocalizedException $e) {
                        $response['API_ERROR']['set'] = [
                            'external_id' => $data['set'][2]['external_id'],
                            'comment' => 'Error with validation attribute sets (' . $e->getMessage() . ')',
                            'magento_id' => null
                        ];
                    }
                } else {
                    //fetch only attribute set ID
                    $set_id = $data['set']['magento_id'];
                    $response['set'] = [
                        'external_id' => $data['set']['external_id'],
                        'comment' => 'Attribute set already exists',
                        'magento_id' => $set_id
                    ];
                }

                if (isset($set_id) && !empty($data['set']['groups'])) {
                    foreach ($data['set']['groups'] as $group) {
                        if (empty($group['magento_id'])) {
                            //create new attribute group
                            if (!empty($group['groupName']) && !empty($group['data'])) {
                                try {
                                    $group_id = $this->groupAdd($set_id, $group['groupName'], $group['data']);

                                    $response['set']['groups'][$group_id] = [
                                        'external_id' => $group['data']['external_id'],
                                        'comment' => 'Attribute group `' . $group['groupName'] .'` create successfully',
                                        'magento_id' => $group_id
                                    ];
                                } catch (\Exception $e) {
                                    $response['API_ERROR']['set']['groups'][] = [
                                        'external_id' => $group['data']['external_id'],
                                        'comment' => $e->getMessage(),
                                        'magento_id' => null
                                    ];
                                } catch (\Magento\Framework\Exception\LocalizedException $e) {
                                    $response['API_ERROR']['set']['groups'][] = [
                                        'external_id' => $group['data']['external_id'],
                                        'comment' => $e->getMessage(),
                                        'magento_id' => null
                                    ];
                                }
                            }
                        } else {
                            //group already exists
                            $group_id = $group['magento_id'];
                            $response['set']['groups'][$group_id] = [
                                'external_id' => $group['external_id'],
                                'comment' => 'Attribute group already exists',
                                'magento_id' => $group_id
                            ];
                        }

                        if ($group_id && !empty($group['attributes']) && is_array($group['attributes'])) {
                            $response['set']['groups'][$group_id]['attributes'] = [];
                            //loop through attributes, create them and link to the set
                            foreach ($group['attributes'] as $attribute) {
                                $attribute_id = null;
                                if (empty($attribute['magento_id'])) {
                                    //new attribute
                                    try {
                                        $attribute_api_model = ObjectManager::getInstance()->create(
                                            'ICEShop\ICECatConnect\Model\ICECatConnectCatalogProductAttributeApi'
                                        );
                                        $attribute_id = $attribute_api_model->create($attribute);
                                        $response['set']['groups'][$group_id]['attributes']
                                        [$attribute['external_id']] = [
                                            'external_id' => $attribute['external_id'],
                                            'comment' => 'Attribute created successfully',
                                            'magento_id' => $attribute_id,
                                            'attribute_code' => $attribute['attribute_code'],
                                            'type' => $attribute['frontend_input']
                                        ];
                                    } catch (\Exception $e) {
                                        $response['API_ERROR']['set']['groups'][$group_id]['attributes']
                                        [$attribute['external_id']] = [
                                            'external_id' => $attribute['external_id'],
                                            'comment' => 'Error with attribute: `' . $e->getMessage() . '`',
                                            'magento_id' => null
                                        ];
                                    } catch (\Magento\Framework\Exception\LocalizedException $e) {
                                        $response['API_ERROR']['set']['groups'][$group_id]['attributes']
                                        [$attribute['external_id']] = [
                                            'external_id' => $attribute['external_id'],
                                            'comment' => 'Error with attribute: `' . $e->getMessage() . '`',
                                            'magento_id' => null
                                        ];
                                    }
                                } else {
                                    //attribute already exists
                                    $attribute_id = $attribute['magento_id'];
                                    $response['set']['groups'][$group_id]['attributes'][$attribute['external_id']] = [
                                        'external_id' => $attribute['external_id'],
                                        'comment' => 'Attribute already exists',
                                        'magento_id' => $attribute_id,
                                        'attribute_code' => $attribute['attribute_code'],
                                        'type' => $attribute['field_type']
                                    ];
                                }
                                //link attribute to attribute set
                                $catalogProductApi = ObjectManager::getInstance()->create(
                                    'ICEShop\ICECatConnect\Model\ICECatConnectCatalogProductAttributeApi'
                                );
                                if (!empty($attribute_id)) {
                                    try {
                                        if (array_key_exists('sort_order', $attribute)) {
                                            $res = $catalogProductApi->attributeAdd(
                                                $attribute_id,
                                                $set_id,
                                                $group_id,
                                                $attribute['sort_order']
                                            );
                                        } else {
                                            $res = $catalogProductApi->attributeAdd($attribute_id, $set_id, $group_id);
                                        }
                                    } catch (\Exception $e) {
                                        $reponse['API_ERROR']['set']['groups'][$group_id]['attributes'][$attribute_id]
                                        ['set_attribute_add'] = [
                                            'comment' => $e->getMessage()
                                        ];
                                    } catch (\Magento\Framework\Exception\LocalizedException $e) {
                                        $reponse['API_ERROR']['set']['groups'][$group_id]['attributes'][$attribute_id]
                                        ['set_attribute_add'] = [
                                            'comment' => $e->getMessage()
                                        ];
                                    }
                                }

                                //save all options
                                if (!empty($attribute_id) && !empty($attribute['options'])) {
                                    foreach ($attribute['options'] as $option) {
                                        try {
                                            if (empty($option['magento_id'])) {
                                                $option_id = $catalogProductApi->addOption($attribute_id, $option);

                                                $response['set']['groups'][$group_id]['attributes']
                                                [$attribute['external_id']]['options'][] = [
                                                    'comment' => 'Option added successfully',
                                                    'item' => [
                                                        'value' => $option_id,
                                                        'external_id' => $option['external_id']
                                                    ]
                                                ];
                                            } else {
                                                $option_id = $option['magento_id'];
                                                $catalogProductApi->updateOption($attribute_id, $option_id, $option);

                                                $response['set']['groups'][$group_id]['attributes']
                                                [$attribute['external_id']]['options'][] = [
                                                    'comment' => 'Option updated successfully',
                                                    'item' => [
                                                        'value' => $option_id,
                                                        'external_id' => $option['external_id']
                                                    ]
                                                ];
                                            }
                                        } catch (\Exception $e) {
                                            $response['API_ERROR']['set']['groups'][$group_id]['attributes']
                                            [$attribute['external_id']]['options'][] = [
                                                'comment' => $e->getMessage(),
                                                'item' => $option
                                            ];
                                        } catch (\Magento\Framework\Exception\LocalizedException $e) {
                                            $response['API_ERROR']['set']['groups'][$group_id]['attributes']
                                            [$attribute['external_id']]['options'][] = [
                                                'comment' => $e->getMessage(),
                                                'item' => $option
                                            ];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return json_encode($response);
        }
        return false;
    }

    /**
     * Add attribute group to attribute set
     *
     * @param $attributeSetId
     * @param $groupName
     * @param array $data
     * @return int
     * @throws \Exception
     */
    public function groupAdd($attributeSetId, $groupName, $data = [])
    {
        $groups_arr = [];
        $groups = ObjectManager::getInstance()->get(
            '\Magento\Eav\Model\ResourceModel\Entity\Attribute\Group\Collection'
        )
            ->setAttributeSetFilter($attributeSetId)
            ->load();

        foreach ($groups as $group) {
            $groups_arr[$group->getId()] = $group->getAttributeGroupName();
        }
        if ($group_id = array_search($groupName, $groups_arr)) {
            if (!empty($data['external_id'])) {
                $this->_saveConversions($data['external_id'], $group_id, 'attribute_group');
            }
            return (int)$group_id;
        }

        $group = ObjectManager::getInstance()->create('Magento\Eav\Model\Entity\Attribute\Group');
        $group->setAttributeSetId($attributeSetId)
            ->setAttributeGroupName(
                $groupName
            );

        if (!empty($data['sort_order'])) {
            $group->setSortOrder((int)$data['sort_order']);
        }

        if ($group->itemExists()) {
            $this->_fault('group_already_exists');
        }
        try {
            $group->save();
            if (!empty($data['external_id'])) {
                $this->_saveConversions($data['external_id'], $group->getId(), 'attribute_group');
            }
        } catch (\Exception $e) {
            $this->_fault('group_add_error', $e->getMessage());
        }
        return (int)$group->getId();
    }

    public function saveProductsBatch($data)
    {
        $this->_getConnection();
        $this->_getResource();
        $this->_dataDecode($data);
        $response = [];
        if (!empty($data) && is_array($data)) {
            $products_api_model = ObjectManager::getInstance()->create(
                'ICEShop\ICECatConnect\Model\ICECatConnectCatalogProductAttributeApi'
            );
            foreach ($data as $product) {
                try {
                    if (array_key_exists('product', $product) && !empty($product['product'])) {
                        foreach ($product['product'] as $store_id => $product_data) {
                            $products_api_model->update(
                                $product_data['product_id'],
                                $product_data['attribute_data'],
                                $store_id
                            );
                            try {
                                $write = $this->connection;
                                $write->query("UPDATE `" . $this->resource->getTableName('catalog_product_entity'). "` 
                                SET `updated_ice` = NOW() WHERE `entity_id` = " . $product_data['product_id']);
                            } catch (\Exception $e) {
                            }

                            $response[$product['product_id']][] = [
                                'comment' => 'Product updated successfully',
                                'product_id' => $product_data['product_id'],
                                'store_id' => $store_id
                            ];
                        }
                    }

                    if (isset($product['links']) && is_array($product['links'])) {
                        if (isset($product['links']['related']) && is_array($product['links']['related'])) {
                            try {
                                $result = $this->_saveProductsLinks(
                                    $product['product_id'],
                                    $product['links']['related'],
                                    'cross_sell'
                                );
                                $response[$product['product_id']]['cross_sell'] = $result;
                            } catch (\Exception $e) {
                                $response['API_ERROR'][$product['product_id']]['cross_sell'] = [
                                    'comment' => $e->getMessage(),
                                    'status' => 'Error while saving "cross sell" links',
                                    'product_id' => $product['product_id']
                                ];
                            }
                        }

                        if (isset($product['links']['preferred']) && is_array($product['links']['preferred'])) {
                            try {
                                $result = $this->_saveProductsLinks(
                                    $product['product_id'],
                                    $product['links']['preferred'],
                                    'related'
                                );
                                $response[$product['product_id']]['related'] = $result;
                            } catch (\Exception $e) {
                                $response['API_ERROR'][$product['product_id']][] = [
                                    'comment' => $e->getMessage(),
                                    'status' => 'Error while saving "related" links',
                                    'product_id' => $product['product_id']
                                ];
                            }
                        }

                        if (isset($product['links']['alternatives']) && is_array($product['links']['alternatives'])) {
                            try {
                                $result = $this->_saveProductsLinks(
                                    $product['product_id'],
                                    $product['links']['alternatives'],
                                    'up_sell'
                                );
                                $response[$product['product_id']]['up_sell'] = $result;
                            } catch (\Exception $e) {
                                $response['API_ERROR'][$product['product_id']][] = [
                                    'comment' => $e->getMessage(),
                                    'status' => 'Error while saving "up sell" links',
                                    'product_id' => $product['product_id']
                                ];
                            }
                        }
                    }
                } catch (\Exception $e) {
                    $response['API_ERROR'][$product_data['product_id']] = [
                        'comment' => $e->getMessage(),
                        'exception' => [
                            'class' => get_class($e),
                            'file' => $e->getFile(),
                            'line' => $e->getLine()
                        ],
                        'status' => 'Error while updating the product',
                        'product_id' => $product['product_id'],
                        'item' => $product_data,
                    ];
                }
            }
        }
        return json_encode($response);
    }

    /**
     * Working with products links - deleting and adding if needed
     */
    public function _saveProductsLinks($product_id, $relation_arr, $relation_type)
    {
        $this->_getConnection();
        $response = [];
        $relation_types = [
            'related',
            'up_sell',
            'cross_sell'
        ];

        if (!empty($product_id) && is_array($relation_arr) && in_array($relation_type, $relation_types)) {
            $product_link_api_model = ObjectManager::getInstance()->create(
                'ICEShop\ICECatConnect\Model\ICECatConnectCatalogProductLink'
            );
            try {
                $links_raw = $product_link_api_model->items($relation_type, $product_id, null, true);
                $links = [];
                foreach ($links_raw as $link_id => $link_item) {
                    if ((isset($link_item['is_icecat']) && $link_item['is_icecat'] == 1)) {
                        $links[] = $link_item[0];
                    }
                }
                $links_to_add = array_diff($relation_arr, $links);
                $links_to_delete = array_diff($links, $relation_arr);

                if (!empty($links_to_delete)) {
                    $link = implode(',', $links_to_delete);
                    try {
                        $product_link_api_model->remove($relation_type, $product_id, $links_to_delete);
                        $response[] = [
                            'comment' => 'Link deleted successfully',
                            'item' => [
                                'product_id' => $product_id,
                                'related_product_id' => $link
                            ]
                        ];
                    } catch (\Exception $e) {
                        $response['API_ERROR'][] = [
                            'comment' => $e->getMessage(),
                            'status' => 'Error while deleting link item',
                            'item' => [
                                'product_id' => $product_id,
                                'related_product_id' => $link
                            ]
                        ];
                    }
                }

                if (!empty($links_to_add)) {
                    $link_data = ['is_icecat' => '1'];
                    $link = implode(',', $links_to_add);
                    try {
                        $product_link_api_model->assign($relation_type, $product_id, $links_to_add, $link_data);
                        $response[] = [
                            'comment' => 'Link added successfully',
                            'item' => [
                                'product_id' => $product_id,
                                'related_product_id' => $link
                            ]
                        ];
                    } catch (\Exception $e) {
                        $response['API_ERROR'][] = [
                            'comment' => $e->getMessage(),
                            'status' => 'Error while adding link item',
                            'item' => [
                                'product_id' => $product_id,
                                'related_product_id' => $link
                            ]
                        ];
                    }
                }
            } catch (\Exception $e) {
                $response['API_ERROR'][] = [
                    'comment' => $e->getMessage(),
                    'status' => 'Error while fetching links',
                    'item' => $product_id
                ];
            }
        }
        return $response;
    }

    public function queueProductsImages($data)
    {
        $this->_getConnection();
        $this->_dataDecode($data);

        $write_resource = $this->connection;
        $response = [];
        if (!empty($data) && is_array($data)) {
            //reset all updated products images
            $updated_products = array_keys($data);
            $fields = [];
            $fields['deleted'] = 1;
            $fields['is_default'] = 0;
            $where = $write_resource->quoteInto('product_id IN (?)', $updated_products);
            try {
                $write_resource->update('icecat_products_images', $fields, $where);
            } catch (\Exception $e) {
                $response['API_ERROR'] = [
                    'comment' => $e->getMessage(),
                    'status' => 'Error while setting images as deleted'
                ];
                return $response;
            }

            //update images
            foreach ($data as $product) {
                if (empty($product['images'])) {
                    continue;
                }

                try {
                    foreach ($product['images'] as $image) {
                        $table_name = $write_resource->quoteIdentifier('icecat_products_images');
                        if (empty($image['default'])) {
                            $image['default'] = '0';
                        } else {
                            $image['default'] = '1';
                        }
                        $fields = [
                            "product_id" => $product['product_id'],
                            "external_url" => $image['external_url'],
                            "is_default" => $image['default'],
                            "deleted" => 0
                        ];

                        $sql = "INSERT INTO $table_name (product_id, external_url, is_default) 
                        VALUES (" . $fields['product_id'] . ", '" . $fields['external_url'] . "', "
                            . $fields['is_default'] . ") 
                        ON DUPLICATE KEY UPDATE deleted = " . $fields['deleted'] . ", is_default = "
                            . $fields['is_default'] . "";

                        $write_resource->query($sql);

                        $response[] = [
                            'comment' => 'Added successfully',
                            'item' => $image,
                            'product_id' => $product['product_id']
                        ];
                    }
                } catch (\Exception $e) {
                    $response['API_ERROR'][] = [
                        'comment' => $e->getMessage(),
                        'item' => $image,
                        'product_id' => $product['product_id']
                    ];
                }
            }
        }
        return json_encode($response);
    }

    /**
     * @param $data
     * @return array|bool
     */
    public function updateLanguageCodes($data)
    {
        $this->_getConnection();
        $this->_getResource();
        $this->_dataDecode($data);

        $table_name = $this->resource->getTableName('core_config_data');
        if (!empty($data) && !empty($data['language_codes'])) {
            array_unshift($data['language_codes'], ['value' => '-1', 'label' => '--Choose the language--']);
            $value = serialize($data['language_codes']);
            try {
                $result = [];
                $sql = "SELECT `config_id` FROM " . $table_name . " WHERE path = 'iceshop_default_icecat_languages'";
                $query = $this->connection->query($sql);
                while ($row = $query->fetch()) {
                    $result[] = $row;
                }
                if (isset($result[0]['config_id'])) {
                    $sql = "UPDATE " . $table_name . " SET value = '" . $value . "' WHERE config_id = '"
                        . $result[0]['config_id'] . "';";
                } else {
                    $sql = "INSERT INTO " . $table_name . " (scope, scope_id, path, value) 
                    VALUES ('default','0','iceshop_default_icecat_languages','" . $value . "');";
                }
                $this->connection->query($sql);
                return json_encode([
                    'comment' => 'Language codes updated',
                    'data' => json_encode($data)
                ]);
            } catch (\Exception $e) {
                return json_encode([
                    'comment' => $e->getMessage(),
                    'status' => 'Error while saving language codes'
                ]);
            }
        }
        return false;
    }

    /**
     * @param $path
     * @param $data
     * @return bool
     * @throws Exception
     */
    public function _saveConfig($path, $data)
    {
        if (!empty($path) && !empty($data)) {
            $store_config = ObjectManager::getInstance()->create('\Magento\Config\Model\ResourceModel\Config');
            $store_config->saveConfig($path, $data, 'default', 0);
        } else {
            return false;
        }
    }

    /**
     * @param $data
     * @return array
     */
    public function setIndexerMode($data)
    {

        $this->_getConnection();
        $this->_dataDecode($data);

        //save previous state before any change
        $indexingProcesses = ObjectManager::getInstance()->create('Magento\Indexer\Model\Indexer\Collection');
        if (!empty($indexingProcesses)) {
            $current_state = [];
            foreach ($indexingProcesses as $process) {
                $current_state[$process->getId()] = $process->isScheduled();
            }
        }
        if (!empty($data['mode'])) {
            if ($data['mode'] == $this::INDEXER_MODE_SCHEDULED) {
                foreach ($indexingProcesses as $process) {
                    $process->setScheduled(true);
                }
            }

            if ($data['mode'] == $this::INDEXER_MODE_UPDATE_ON_SAVE) {
                foreach ($indexingProcesses as $process) {
                    $process->setScheduled(false);
                }
            }
        }
        if (!empty($current_state)) {
            return json_encode(['modes' => $current_state]);
        }
    }

    /**
     * Run full reindex of shops indexing processes
     *
     * @return string
     */
    public function runFullReindex()
    {
        try {
            $this->getConfigWriter();
            $this->configWriter->save('icecatconnect_content_last_finish', time());

            $indexer = ObjectManager::getInstance()->create('Magento\Indexer\Model\Indexer');
            foreach ($indexer as $process) {
                $process->reindexAll();
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return 'Reindex running';
    }


    public function lastInsertId($tableName = null, $primaryKey = null)
    {
        $this->_getConnection();
        return $this->connection->lastInsertId();
    }


    /**
     * Start uploading imaages that was added to queue
     *
     * @param mixed $data
     * @return array|string
     */

    public static function checkVersion()
    {
        $productMetadata = ObjectManager::getInstance()->get('Magento\Framework\App\ProductMetadataInterface');
        $version = $productMetadata->getVersion();
        $checkVersion = '';
        if ((string)substr($version, 0, 3) == '2.0') {
            $checkVersion = '2.0';
        }
        return $checkVersion;
    }

    public function processProductsImagesQueue($data)
    {
        $this->_getConnection();
        $this->_getResource();

        $read_resource = $this->connection;
        $write_resource = $this->connection;

        $this->_dataDecode($data);

        $image_counter = 0;
        $response = [];

        $productRepository = ObjectManager::getInstance()->create('Magento\Catalog\Api\ProductRepositoryInterface');
        $select = $read_resource
            ->select()
            ->from('icecat_products_images', '*')
            ->where($read_resource->quoteInto('deleted=?', '0'))
            ->where($read_resource->quoteInto('broken=?', '0'))
            ->where($read_resource->quoteInto('internal_url=?', ''));

        if (!empty($data)) {
            /**
             * work with limit, download specified amount of images,
             * don't cause server speed issues
             */
            $batch_size = !empty($data['batch_size']) ? ceil($data['batch_size']) : 100;
            $select->limit($batch_size);
        }
        $query = $select->query();
        try {
            $images_to_download = [];
            while ($row = $query->fetch()) {
                $images_to_download[] = $row;
            }
        } catch (\Exception $e) {
            $response['API_ERROR'] = [
                'comment' => $e->getMessage(),
                'status' => 'Error while fetching images to download'
            ];
            return $response;
        }

        if (!empty($images_to_download)) {
            $products_cleaned = [];
            $scopeConfig = ObjectManager::getInstance()->get('\Magento\Framework\App\Config\ScopeConfigInterface');
            $replace_images = $scopeConfig->getValue(
                'iceshop_icecatconnect/icecatconnect_service_settings/replace_product_images_by_icecat'
            );

            if (self::checkVersion() == '2.0') {

                $eav_attribute_table_name = $this->resource->getTableName('eav_attribute');
                $catalog_product_entity_media_gallery = $this->resource->getTableName(
                    'catalog_product_entity_media_gallery'
                );
                $catalog_product_entity_media_gallery_value = $this->resource->getTableName(
                    'catalog_product_entity_media_gallery_value'
                );
                $catalog_product_entity_media_gallery_value_to_entity = $this->resource->getTableName(
                    'catalog_product_entity_media_gallery_value_to_entity'
                );
                $media_gallery_select_attribute_id = $this->connection->query(
                    "SELECT attribute_id FROM $eav_attribute_table_name WHERE `attribute_code` = 'media_gallery';"
                );
                $media_gallery_id = $media_gallery_select_attribute_id->fetch()['attribute_id'];
            }

            foreach ($images_to_download as $image) {
                if ($replace_images == 1) {
                    if (!in_array($image['product_id'], $products_cleaned)) {
                        $products_cleaned[] = $image['product_id'];
                        //run the cleanup
                        $product = ObjectManager::getInstance()->create(
                            'ICEShop\ICECatConnect\Model\ICEShopICECatConnectProduct'
                        )->load(
                            $image['product_id']
                        );
                        $productSku = $product->getSku();
                        $galleryManagement = ObjectManager::getInstance()->create(
                            'Magento\Catalog\Model\Product\Gallery\GalleryManagement'
                        );
                        $images_to_delete = [];
                        $allImages = $product->getMediaGalleryImages()->getItems();
                        if (!empty($allImages)) {
                            foreach ($allImages as $image_entity) {
                                $image_data = $image_entity->getData();
                                if (isset($image_data['value_id'])) {
                                    $images_to_delete[] = $image_data['value_id'];
                                }
                            }
                        }

                        if (!empty($images_to_delete)) {
                            foreach ($images_to_delete as $imd) {
                                $result = $galleryManagement->remove($productSku, $imd);
                            }
                        }
                        $this->deleteBaseImage($image['product_id']);
                    }
                }

                $internal_url = false;
                $link = $image['external_url'];
                $image_str = false;
                $size = false;
                try {
                    $size = getimagesize($link);
                    $image_str = file_get_contents($link);
                } catch (\Exception $e) {
                    $response['API_ERROR'][$image['entity_id']][] = [
                        'comment' => 'Image link is broken or file corrupted',
                        'status' => 'Error while downloading image',
                        'item' => $image
                    ];
                }
                if ($image_str == false || $size == false) {
                    $fields = [];
                    $fields['broken'] = $write_resource->quote(1);
                    $where = $write_resource->quoteInto('entity_id = ?', $image['entity_id']);

                    $write_resource->update('icecat_products_images', $fields, $where);
                    $response['API_ERROR'][$image['entity_id']][] = [
                        'comment' => 'Image link is broken or file corrupted',
                        'status' => 'Error while downloading image',
                        'item' => $image
                    ];
                    unset($image, $link, $size, $image_str);
                    continue;
                }
                $types = [];
                $flag_types = false;
                if ($image['is_default'] == 1) {
                    $types = ['small_image', 'thumbnail', 'image'];
                    $flag_types = true;
                }
                $newImage = [
                    'file' => [
                        'content' => base64_encode($image_str),
                        'mime' => $size['mime'],
                        'name' => 'image' . '_' . md5($image['product_id'] . $image['external_url']) . '_' . time()
                    ],
                    'label' => '',
                    'position' => 1,
                    'types' => $types,
                    'exclude' => 0
                ];
                try {
                    $dir = ObjectManager::getInstance()->get('Magento\Framework\Filesystem');
                    $directory_list = ObjectManager::getInstance()->get(
                        '\Magento\Framework\App\Filesystem\DirectoryList'
                    );
                    $mediadir = $dir->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
                    $path = $mediadir->create('icecatconnect');
                    $media_folder = $directory_list->getPath('media');
                    $target_path = $media_folder . DIRECTORY_SEPARATOR . 'icecatconnect';
                    $extension = image_type_to_extension($size[2]);
                    $target_file = $target_path . DIRECTORY_SEPARATOR . $newImage['file']['name'] . $extension;

                    if (file_exists($target_path)) {
                        file_put_contents($target_file, $image_str);
                    }

                    $product_ = ObjectManager::getInstance()->create(
                        'ICEShop\ICECatConnect\Model\ICEShopICECatConnectProduct'
                    )->load(
                        $image['product_id']
                    );
                    $product_->addImageToMediaGallery($target_file, $types, false, false, true);
                    if ($flag_types) {
                        $product_->setImage($product_->currentUploadFileName)->setSmallImage(
                            $product_->currentUploadFileName
                        )->setThumbnail($product_->currentUploadFileName);
                    }
                    if ($product_->save()) {
                        $internal_url = $product_->currentUploadFileName;
                        unlink($target_file);
                        $response[$image['entity_id']][] = [
                            'status' => 'Image was created successfully',
                            'item' => $image
                        ];
                    } else {
                        $response['API_ERROR'][$image['entity_id']][] = [
                            'comment' => 'Images not link to product: (' . $target_file . ')',
                            'status' => 'Error while creating image attribute',
                            'item' => $image
                        ];
                    }
                    $image_counter++;
                } catch (\Exception $e) {
                    $response['API_ERROR'][$image['entity_id']][] = [
                        'comment' => $e->getMessage(),
                        'status' => 'Error while creating image attribute',
                        'item' => $image
                    ];
                } catch (\Magento\Framework\Exception\LocalizedException $e) {
                    $response['API_ERROR'][$image['entity_id']][] = [
                        'comment' => $e->getMessage(),
                        'status' => 'Error while creating image attribute',
                        'item' => $image
                    ];
                }

                if (!empty($internal_url)) {
                    $fields = [];
                    $fields['internal_url'] = $internal_url;
                    $where = $write_resource->quoteInto('entity_id = ?', $image['entity_id']);

                    try {
                        $write_resource->update('icecat_products_images', $fields, $where);

                        $response[$image['entity_id']][] = [
                            'status' => 'Image was added successfully',
                            'item' => $image
                        ];
                    } catch (\Exception $e) {
                        $response['API_ERROR'][$image['entity_id']][] = [
                            'comment' => $e->getMessage(),
                            'status' => 'Error while saving data to mapping table',
                            'item' => $image
                        ];
                    }
                }

                if (self::checkVersion() == '2.0') {

                    $entity_id = $image['product_id'];
                    $store_id = $this->_getStoreId();

                    $this->connection->query("INSERT INTO $catalog_product_entity_media_gallery 
(`attribute_id`, `value`) VALUES('$media_gallery_id', '$internal_url');");
                    $insert_id = $this->lastInsertId($catalog_product_entity_media_gallery, 'value_id');

                    $this->connection->query("INSERT INTO $catalog_product_entity_media_gallery_value 
(`value_id`,`store_id`,`entity_id`, `position`) VALUES('$insert_id', '$store_id', '$entity_id', '$insert_id');");
                    $this->connection->query("INSERT INTO $catalog_product_entity_media_gallery_value 
(`value_id`,`store_id`,`entity_id`, `position`) VALUES('$insert_id', 0, '$entity_id', 0);");
                    $this->connection->query("INSERT INTO $catalog_product_entity_media_gallery_value_to_entity 
(`value_id`, `entity_id`) VALUES('$insert_id', '$entity_id');");

                    $base_path = ObjectManager::getInstance()->get('\Magento\Framework\App\Filesystem\DirectoryList')
                            ->getPath('media').DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR.'catalog'.
                        DIRECTORY_SEPARATOR.'product'.DIRECTORY_SEPARATOR;
                    $dest_path = ObjectManager::getInstance()
                            ->get('\Magento\Framework\App\Filesystem\DirectoryList')
                            ->getPath('media').DIRECTORY_SEPARATOR.'catalog'.DIRECTORY_SEPARATOR .'product'.
                        DIRECTORY_SEPARATOR;
                    $imagePathBase = $base_path.$internal_url;
                    $imagePathDest = $dest_path.$internal_url;

                    if (file_exists($imagePathBase)) {
                        copy($imagePathBase, $imagePathDest);
                        unlink($imagePathBase);
                    }
                }
                unset($image, $link, $size, $image_str, $types, $newImage, $internal_url);
            }
        }
        $response['image_counter'] = $image_counter;
        return json_encode($response);
    }

    /**
     * Delete image that was as set as base image in shop by frontend
     *
     * @param $product_id
     */
    function deleteBaseImage($product_id)
    {
        $mediaPath = BP . DIRECTORY_SEPARATOR . 'pub' . DIRECTORY_SEPARATOR . 'media'
            . DIRECTORY_SEPARATOR . 'catalog' . DIRECTORY_SEPARATOR . 'product';
        $this->_getConnection();
        $this->_getResource();

        $product = ObjectManager::getInstance()->create('ICEShop\ICECatConnect\Model\ICEShopICECatConnectProduct')
            ->load($product_id);
        $images_to_delete = [];
        $allImages = $product->getMediaGalleryImages()->getItems();
        if (!empty($allImages)) {
            foreach ($allImages as $image_entity) {
                $image_data = $image_entity->getData();
                if (isset($image_data['value_id'])) {
                    $images_to_delete[] = $image_data['value_id'];
                }
            }
        }
        if (!empty($images_to_delete)) {
            $getData = [];
            $select = "SELECT value FROM {$this->resource->getTableName('catalog_product_entity_media_gallery')} 
WHERE value_id IN (" . implode(',', $images_to_delete) . ")";

            $query = $select->query();
            while ($row = $query->fetch()) {
                $getData[] = $row;
            }
            $deleteFilePath = false;
            if (!empty($getData)) {
                foreach ($getData as $data) {
                    if (isset($data['value'])) {
                        $deleteFilePath[] = $data['value'];
                    }
                }
            }
            $sql = "DELETE FROM {$this->resource->getTableName('catalog_product_entity_media_gallery')} 
WHERE value_id IN (" . implode(',', $images_to_delete) . ")";
            $exec = $this->connection->query($sql);

            if ($exec) {
                if (!empty($deleteFilePath)) {
                    foreach ($deleteFilePath as $file) {
                        $target = $mediaPath . $file;
                        if (file_exists($target)) {
                            $result = unlink($target);
                        }
                    }
                }
            }
        }
    }

    /**
     * Try upload broken images (that have flag  broken = 1)
     * @param mixed $data
     * @return array|string
     */
    public function processBrokenProductsImages($data)
    {
        $this->_getConnection();

        $this->_dataDecode($data);

        $write_resource = $this->connection;
        $image_counter = 0;
        $response = [];
        //result is stored into var for future uses (e.g. error checking etc)
        do {
            $res = $this->cleanProductsImagesQueue($data);
        } while (!empty($res) && array_key_exists('images_counter', $res) && $res['images_counter'] > 0);

        $select = $write_resource
            ->select()
            ->from('icecat_products_images', '*')
            ->where($write_resource->quoteInto('broken=?', '1'));

        if (!empty($data)) {
            /**
             * work with limit, download specified amount of images,
             * don't cause server speed issues
             */
            $batch_size = !empty($data['batch_size']) ? ceil($data['batch_size']) : 100;
            $select->limit($batch_size);
        }
        $query = $select->query();
        try {
            $images_to_download = [];
            while ($row = $query->fetch()) {
                $images_to_download[] = $row;
            }
        } catch (\Exception $e) {
            $response['API_ERROR'] = [
                'comment' => $e->getMessage(),
                'status' => 'Error while fetching images to process'
            ];
            return $response;
        }
        if (!empty($images_to_download)) {
            foreach ($images_to_download as $image) {
                $link = $image['external_url'];
                try {
                    $size = getimagesize($link);
                    $image_str = file_get_contents($link);
                } catch (\Exception $e) {
                    $response['API_ERROR'][$image['entity_id']][] = [
                        'comment' => 'Image link is broken or file corrupted',
                        'status' => 'Error while downloading image',
                        'item' => $image
                    ];
                }
                if ($image_str == false) {
                    $response['API_ERROR'][$image['entity_id']][] = [
                        'comment' => 'Image link is still broken or file corrupted',
                        'status' => 'Error while downloading image',
                        'item' => $image
                    ];
                    unset($image, $link, $size, $image_str);
                    continue;
                }

                $types = [];
                $flag_types = false;
                if ($image['is_default'] == 1) {
                    $types = ['small_image', 'thumbnail', 'image'];
                    $flag_types = true;
                }
                $newImage = [
                    'file' => [
                        'content' => base64_encode($image_str),
                        'mime' => $size['mime'],
                        'name' => 'image' . '_' . md5($image['product_id'] . $image['external_url']) . '_' . time()
                    ],
                    'label' => '',
                    'position' => 1,
                    'types' => $types,
                    'exclude' => 0
                ];

                $dir = ObjectManager::getInstance()->get('Magento\Framework\Filesystem');
                $directory_list = ObjectManager::getInstance()->get('\Magento\Framework\App\Filesystem\DirectoryList');
                $mediadir = $dir->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
                $path = $mediadir->create('icecatconnect');
                $media_folder = $directory_list->getPath('media');
                $target_path = $media_folder . DIRECTORY_SEPARATOR . 'icecatconnect';
                $extension = image_type_to_extension($size[2]);
                $target_file = $target_path . DIRECTORY_SEPARATOR . $newImage['file']['name'] . $extension;
                if (file_exists($target_path)) {
                    file_put_contents($target_file, $image_str);
                }

                $product_ = ObjectManager::getInstance()->create(
                    'ICEShop\ICECatConnect\Model\ICEShopICECatConnectProduct'
                )->load(
                    $image['product_id']
                );
                $product_->addImageToMediaGallery($target_file, $types, false, false, true);
                if ($flag_types) {
                    $product_->setImage($product_->currentUploadFileName)->setSmallImage(
                        $product_->currentUploadFileName
                    )->setThumbnail($product_->currentUploadFileName);
                }
                if ($product_->save()) {
                    $internal_url = $product_->currentUploadFileName;
                    unlink($target_file);
                    $response[$image['entity_id']][] = [
                        'status' => 'Image was fixed successfully',
                        'item' => $image
                    ];
                } else {
                    $response['API_ERROR'][$image['entity_id']][] = [
                        'comment' => 'Images not link to product: (' . $target_file . ')',
                        'status' => 'Error while creating image attribute in broken function',
                        'item' => $image
                    ];
                }

                if (!empty($internal_url)) {
                    $fields = [];
                    $fields['internal_url'] = $internal_url;
                    $fields['broken'] = 0;
                    $where = $write_resource->quoteInto('entity_id = ?', $image['entity_id']);

                    try {
                        $write_resource->update('icecat_products_images', $fields, $where);

                        $response[$image['entity_id']][] = [
                            'status' => 'Image was added successfully',
                            'item' => $image
                        ];
                    } catch (\Exception $e) {
                        $response['API_ERROR'][$image['entity_id']][] = [
                            'comment' => $e->getMessage(),
                            'status' => 'Error while saving data to mapping table',
                            'item' => $image
                        ];
                    }
                }
                unset($image, $link, $size, $image_str, $types, $newImage, $internal_url);
            }
        }
        $response['image_counter'] = $image_counter;
        return json_encode($response);
    }

    /**
     * @param bool|array $data
     *
     * @return array
     */
    public function cleanProductsImagesQueue($data = false)
    {
        //creating resources
        $resource = $this->_getResource();
        $write_resource = $this->connection;
        $response = [];
        $images_counter = 0;
        $galleryManagement = ObjectManager::getInstance()->create(
            'Magento\Catalog\Model\Product\Gallery\GalleryManagement'
        );
        $mediaPath = BP . DIRECTORY_SEPARATOR . 'pub' . DIRECTORY_SEPARATOR . 'media'
            . DIRECTORY_SEPARATOR . 'catalog' . DIRECTORY_SEPARATOR . 'product';

        $select = $write_resource
            ->select()
            ->from('icecat_products_images', '*')
            ->where($write_resource->quoteInto('deleted=?', '1'));
        if (!empty($data)) {
            /**
             * work with limit, download specified amount of images,
             * don't cause server speed issues
             */
            $batch_size = !empty($data['batch_size']) ? ceil($data['batch_size']) : 100;
            $select->limit($batch_size);
        }
        $query = $select->query();
        try {
            $images_to_delete = [];
            while ($row = $query->fetch()) {
                $images_to_delete[] = $row;
            }
        } catch (\Exception $e) {
            $response['API_ERROR'] = [
                'comment' => $e->getMessage(),
                'status' => 'Error while fetching images marked as deleted'
            ];
            return $response;
        }
        if (!empty($images_to_delete) && is_array($images_to_delete)) {
            foreach ($images_to_delete as $image) {
                $products_media_api_model = ObjectManager::getInstance()->create('Magento\Catalog\Model\Product');
                if ($image['internal_url'] != '') {
                    try {
                        $select_media = $this->connection->query("SELECT value_id FROM {$resource->getTableName(
                            'catalog_product_entity_media_gallery'
                            )} WHERE value = '" . $image['internal_url'] . "';");

                        $select_media = [];
                        while ($row = $select_media->fetch()) {
                            $select_media[] = $row;
                        }

                        if (!empty($select_media)) {
                            foreach ($select_media as $value) {
                                $sku = $products_media_api_model->load($image['product_id'])->getSku();
                                if (isset($sku) && isset($value['value_id'])) {
                                    $galleryManagement->remove($sku, $value['value_id']);
                                }
                            }
                        }
                        try {
                            $removeFile = $mediaPath . $image['internal_url'];
                            if ((file_exists($removeFile)) && (is_file($removeFile))) {
                                @unlink($removeFile);
                            }
                        } catch (\Exception $e) {
                            $response['API_ERROR'][] = [
                                'comment' => 'Error with unlink file (' . $e->getMessage() . ')',
                                'db_row' => $image
                            ];
                        }
                    } catch (\Exception $e) {
                        // If image is not presents for the product
                        if ($e->getMessage() != 'not_exists') {
                            //logging result
                            $response['API_ERROR'][] = [
                                'comment' => $e->getMessage(),
                                'db_row' => $image
                            ];
                            continue;
                        }
                    }
                }
                try {
                    $delete_result = $write_resource->delete(
                        'icecat_products_images',
                        [
                            $write_resource->quoteInto('entity_id = ?', $image['entity_id']),
                            $write_resource->quoteInto('deleted=?', '1')
                        ]
                    );

                    //logging result
                    $response[] = [
                        'comment' => 'Deleted successfully',
                        'db_row' => $image
                    ];
                    $images_counter++;
                    continue;
                } catch (\Exception $e) {
                    //logging result
                    $response['API_ERROR'][] = [
                        'comment' => $e->getMessage(),
                        'db_row' => $image
                    ];
                    continue;
                }
            }
        }
        $response['images_counter'] = $images_counter;
        return $response;
    }

    public function _getResource()
    {
        if (!$this->resource) {
            $this->resource = ObjectManager::getInstance()->create('\Magento\Framework\App\ResourceConnection');
        }
        return $this->resource;
    }

    public function fixFailedAttributes()
    {
        $this->_getResource();
        $write_resource = $this->_getConnection();
        $response = [];

        $sql = "
        DELETE
        e.*
        FROM {$this->resource->getTableName('catalog_product_entity_int')} e
        INNER JOIN {$this->resource->getTableName('eav_attribute')} a 
        ON e.`attribute_id` = a.`attribute_id` AND attribute_code LIKE 'i\_%\_1'
        INNER JOIN {$this->resource->getTableName('eav_attribute_option')} o ON e.`value` = o.`option_id`
        WHERE o.`attribute_id` <> a.`attribute_id`;";
        try {
            $write_resource->query($sql);
            $response['succes'] = 1;
        } catch (\Exception $e) {
            $response['API_ERROR'] = [
                'comment' => $e->getMessage(),
                'status' => 'Error during cleanup of failed attributes.'
            ];
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $response['API_ERROR'] = [
                'comment' => $e->getMessage(),
                'status' => 'Error during cleanup of failed attributes.'
            ];
        }
        return json_encode($response);
    }

    public function filtersMap($key = false, $value = false, $get = true)
    {
        if (!empty($key)) {
            if ($get == true) {
                return !empty($this->arFiltersMap[$key]) ? $this->arFiltersMap[$key] : false;
            } else {
                $this->arFiltersMap[$key] = $value;
            }
        } else {
            return $this->arFiltersMap;
        }
    }

    /**
     * Parse filters and format them to be applicable for collection filtration
     *
     * @param null|object|array $filters
     * @param array $fieldsMap Map of field names in format: array('field_name_in_filter' => 'field_name_in_db')
     * @return array
     */
    public function parseFilters($filters, $fieldsMap = null)
    {
        // if filters are used in SOAP they must be represented in array format to be used for collection filtration
        if (is_object($filters)) {
            $parsedFilters = [];
            // parse simple filter
            if (isset($filters->filter) && is_array($filters->filter)) {
                foreach ($filters->filter as $field => $value) {
                    if (is_object($value) && isset($value->key) && isset($value->value)) {
                        $parsedFilters[$value->key] = $value->value;
                    } else {
                        $parsedFilters[$field] = $value;
                    }
                }
            }
            // parse complex filter
            if (isset($filters->complex_filter) && is_array($filters->complex_filter)) {
                $parsedFilters += $this->_parseComplexFilter($filters->complex_filter);
            }

            $filters = $parsedFilters;
        }
        // make sure that method result is always array
        if (!is_array($filters)) {
            $filters = [];
        }
        // apply fields mapping
        if (isset($fieldsMap) && is_array($fieldsMap)) {
            foreach ($filters as $field => $value) {
                if (isset($fieldsMap[$field])) {
                    unset($filters[$field]);
                    $field = $fieldsMap[$field];
                    $filters[$field] = $value;
                }
            }
        }
        return $filters;
    }

    /**
     * Parses complex filter, which may contain several nodes, e.g. when user want to fetch orders which were updated
     * between two dates.
     *
     * @param array $complexFilter
     * @return array
     */
    public function _parseComplexFilter($complexFilter)
    {
        $parsedFilters = [];

        foreach ($complexFilter as $filter) {
            if (!isset($filter->key) || !isset($filter->value)) {
                continue;
            }

            list($fieldName, $condition) = [$filter->key, $filter->value];
            $conditionName = $condition->key;
            $conditionValue = $condition->value;
            $this->formatFilterConditionValue($conditionName, $conditionValue);

            if (array_key_exists($fieldName, $parsedFilters)) {
                $parsedFilters[$fieldName] += [$conditionName => $conditionValue];
            } else {
                $parsedFilters[$fieldName] = [$conditionName => $conditionValue];
            }
        }

        return $parsedFilters;
    }

    /**
     * Convert condition value from the string into the array
     * for the condition operators that require value to be an array.
     * Condition value is changed by reference
     *
     * @param string $conditionOperator
     * @param string $conditionValue
     */
    public function formatFilterConditionValue($conditionOperator, &$conditionValue)
    {
        if (is_string($conditionOperator) && in_array($conditionOperator, ['in', 'nin', 'finset'])
            && is_string($conditionValue)
        ) {
            $delimiter = ',';
            $conditionValue = explode($delimiter, $conditionValue);
        }
    }

    /**
     * @return array
     */
    public function processDefaultProductsImages($data)
    {

        $this->_getResource();
        $data = json_encode($data, true);

        $read_resource = $this->_getConnection();
        $response = [];

        $table_name = $this->resource->getTableName('catalog_product_entity');
        $optionId = false;
        $eav = ObjectManager::getInstance()->get('\Magento\Eav\Model\Config');

        $attribute = $eav->getAttribute('catalog_product', 'active_ice')->getData();
        if (isset($attribute['attribute_id'])) {
            $options = $eav->getAttribute('catalog_product', 'active_ice')->getSource()->getAllOptions();
            foreach ($options as $option) {
                if ($option['label'] == 'Yes') {
                    $optionId = $option['value'];
                    break;
                }
            }
        }

        $select = $read_resource
            ->select()
            ->from('icecat_products_images', '*')
            ->where($read_resource->quoteInto('deleted=?', '0'))
            ->where($read_resource->quoteInto('is_default=?', '1'))
            ->where($read_resource->quoteInto('broken=?', '0'))
            ->where($read_resource->quoteInto('internal_url<>?', ''));
        if ($optionId !== false) {
            $select->joinInner(['t1' => $table_name], "icecat_products_images.product_id = t1.entity_id", [])
                ->where("t1.active_ice = '" . $optionId . "'");
        }
        
        if (!empty($data['page_size'])) {
            if (empty($data['page'])) {
                $data['page'] = 1;
            }
            $select->limitPage($data['page'], $data['page_size']);
        }

        $query = $select->query();

        try {

            $default_images = [];
            while ($row = $query->fetch()) {
                $default_images[] = $row;
            }
        } catch (\Exception $e) {

            $response['API_ERROR'] = [
                'comment' => $e->getMessage(),
                'status' => 'Error while fetching default images'
            ];
        }

        if (!empty($default_images)) {
            foreach ($default_images as $image) {
                try {
                    $product_ = ObjectManager::getInstance()->create(
                        'ICEShop\ICECatConnect\Model\ICEShopICECatConnectProduct'
                    )->load(
                        $image['product_id']
                    );
                    $product_->setImage(
                        $product_->currentUploadFileName
                    )->setSmallImage(
                        $product_->currentUploadFileName
                    )->setThumbnail(
                        $product_->currentUploadFileName
                    );
                    $response[$image['entity_id']][] = [
                        'status' => 'Default image updated',
                        'item' => $image
                    ];
                } catch (\Exception $e) {
                    $response['API_ERROR'][$image['entity_id']][] = [
                        'comment' => 'Error with update default image (' . $e->getMessage() . ')',
                        'status' => 'Error while updating default image',
                        'item' => $image
                    ];
                }
                unset($image);
            }
        }
        unset($default_images, $read_resource, $image, $select);
        return json_encode($response);
    }

    /**
     * Import attributes GTIN or MPN/Brand in to Magento shop
     *
     * @param mixed $data
     * @return string
     * @throws \Exception
     */
    public function saveMainAttributesBatch($data)
    {
        $this->_getConnection();
        $this->_getResource();

        $store_obj = ObjectManager::getInstance()->create('\Magento\Store\Model\StoreManagerInterface');
        $allStores = [];
        $allStores[] = 0; //default store
        foreach ($store_obj->getStores() as $str) {
            $allStores[] = $str->getId();
        }

        $response = [];
        $this->_dataDecode($data);
        if (!empty($data) && is_array($data)) {
            $main_attributes_mapping = $this->getICEshopMapping();
            $products_api_model = ObjectManager::getInstance()->create('Magento\Catalog\Model\Product');
            foreach ($data as $product) {
                if (!array_key_exists('product_id', $product)) {
                    continue;
                }
                $product_obj = $products_api_model->load($product['product_id']);
                $type_flag = false;
                if (array_key_exists('ean', $product)) {
                    if ($main_attributes_mapping['ean'] == '') {
                        continue;
                    }
                    $type_flag = 'ean';
                } elseif (array_key_exists('brand_name', $product) && array_key_exists('mpn', $product)) {
                    if ($main_attributes_mapping['brand_name'] == '' || $main_attributes_mapping['mpn'] == '') {
                        continue;
                    }
                    $type_flag = 'brand_mpn';
                }
                if ($type_flag == false) {
                    continue;
                }
                try {
                    switch ($type_flag) {
                        case 'ean':
                            //ean
                            $attribute_code = $main_attributes_mapping['ean'];
                            $product_obj->setData(
                                $attribute_code,
                                $product['ean']
                            );
                            break;

                        case 'brand_mpn':
                            //brand
                            $attribute_code = $main_attributes_mapping['brand_name'];
                            $brand_name_attribute_type = $product_obj->getResource()
                                ->getAttribute($attribute_code)
                                ->getFrontend()
                                ->getInputType();
                            switch ($brand_name_attribute_type) {
                                case 'select':
                                case 'dropdown':
                                    $product_brand_name = $product_obj->getAttributeText($attribute_code);
                                    if (strtolower($product_brand_name) != strtolower($product['brand_name'])) {
                                        $add_option_id = false;
                                        $attribute_ = ObjectManager::getInstance()->create(
                                            'Magento\Eav\Model\Config'
                                        )->getAttribute(
                                            ProductAttributeInterface::ENTITY_TYPE_CODE,
                                            $attribute_code
                                        );
                                        $options = $attribute_->getSource()->getAllOptions();
                                        if (!empty($options)) {
                                            foreach ($options as $key => $value) {
                                                if (!empty($value['label'])) {
                                                    if ($value['label'] == $product['brand_name']) {
                                                        $add_option_id = $value['value'];
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        if ($add_option_id == false) {
                                            $attr = ObjectManager::getInstance()->create(
                                                '\Magento\Eav\Model\ResourceModel\Entity\Attribute'
                                            );
                                            $attrId = $attr->getIdByCode(
                                                ProductAttributeInterface::ENTITY_TYPE_CODE,
                                                $main_attributes_mapping['brand_name']
                                            );
                                            $option = [];
                                            $option['attribute_id'] = $attrId;
                                            $option['value'][$product['brand_name']][0] = $product['brand_name'];
                                            foreach ($allStores as $store) {
                                                $option['value'][$product['brand_name']][$store] =
                                                    $product['brand_name'];
                                            }
                                            ObjectManager::getInstance()->create(
                                                'Magento\Eav\Setup\EavSetup'
                                            )->addAttributeOption($option);

                                            $add_option_id = false;
                                            $attribute_ = ObjectManager::getInstance()->create(
                                                'Magento\Eav\Model\Config'
                                            )
                                                ->getAttribute(
                                                    ProductAttributeInterface::ENTITY_TYPE_CODE,
                                                    $attribute_code
                                                );
                                            $options = $attribute_->getSource()->getAllOptions();
                                            if (!empty($options)) {
                                                foreach ($options as $key => $value) {
                                                    if (!empty($value['label'])) {
                                                        if ($value['label'] == $product['brand_name']) {
                                                            $add_option_id = $value['value'];
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                            if ($add_option_id !== false) {
                                                $product_obj->setData(
                                                    $attribute_code,
                                                    $add_option_id
                                                );
                                            }
                                        } else {
                                            $product_obj->setData(
                                                $attribute_code,
                                                $add_option_id
                                            );
                                        }
                                    }
                                    break;
                                case 'text':
                                    $product_brand_name = $product_obj->getData($attribute_code);
                                    if (empty($product_brand_name)) {
                                        $product_obj->setData(
                                            $attribute_code,
                                            $product['brand_name']
                                        );
                                    }
                                    break;
                            }
                            //mpn
                            $attribute_code = $main_attributes_mapping['mpn'];
                            $product_mpn = $product_obj->getData($attribute_code);
                            if (empty($product_mpn)) {
                                $product_obj->setData(
                                    $attribute_code,
                                    $product['mpn']
                                );
                            } else {
                                if (strtolower($product_mpn) != strtolower($product['mpn'])) {
                                    $product_obj->setData(
                                        $attribute_code,
                                        $product['mpn']
                                    );
                                }
                            }
                            break;
                    }

                    try {
                        if (is_array($errors = $product_obj->validate())) {
                            $strErrors = [];
                            foreach ($errors as $code => $error) {
                                if ($error === true) {
                                    $error = __('Value for "%1" is invalid.', $code)->render();
                                } else {
                                    $error = __('Value for "%1" is invalid: %2', $code, $error)->render();
                                }
                                $strErrors[] = $error;
                            }
                            $this->_fault('data_invalid', implode("\n", $strErrors));
                        }
                        $product_obj->save();
                    } catch (\Exception $e) {
                        $response['API_ERROR'][$product['product_id']] = [
                            'comment' => 'data_invalid : (' . $e->getMessage() . ')',
                            'exception' => [
                                'class' => get_class($e),
                                'file' => $e->getFile(),
                                'line' => $e->getLine(),
                                'trace' => $e->getTraceAsString()
                            ]
                        ];
                    }
                } catch (\Exception $e) {
                    $response['API_ERROR'][$product['product_id']] = [
                        'comment' => $e->getMessage(),
                        'exception' => [
                            'class' => get_class($e),
                            'file' => $e->getFile(),
                            'line' => $e->getLine(),
                            'trace' => $e->getTraceAsString()
                        ],
                        'status' => 'Error while updating the product',
                        'product_id' => $product['product_id']
                    ];
                }
            }
        }
        return json_encode($response);
    }

    /**
     * Update attributes labels, sort order
     *
     * @param mixed $data
     * @return string
     * @throws \Exception
     */
    public function attributesRefresh($data)
    {
        $this->_getConnection();
        $response = [];

        try {
            $source = $data;
            $source = json_decode($source, true);
            if (isset($source['data']) && isset($source['attribute'])) {
                $data = $source['data'];
                $attribute = $source['attribute'];
            } else {
                $response['API_ERROR'] = [
                    'comment' => 'Data not correct',
                ];
            }
            $model = $this->_getAttribute($attribute);
            $entityType = ObjectManager::getInstance()->get(
                '\Magento\Catalog\Model\Product'
            )->getResource()->getEntityType()->getId();
            if ($model->getEntityTypeId() != $entityType) {
                $this->_fault('Entity not same');
            }
            $data['attribute_code'] = $model->getAttributeCode();
            $data['is_user_defined'] = $model->getIsUserDefined();
            $data['frontend_input'] = $model->getFrontendInput();
            $attribute_api_model = ObjectManager::getInstance()->create(
                'ICEShop\ICECatConnect\Model\ICECatConnectCatalogProductAttributeApi'
            );
            $attribute_api_model->prepareDataForSave($data);
            $model->addData($data);
            try {
                $model->save();
                $response[] = [
                    'comment' => 'Success updated',
                ];
                // clear translation cache because attribute labels are stored in translation
            } catch (\Exception $e) {
                $response['API_ERROR'] = [
                    'comment' => 'unable_to_save (' . $e->getMessage() . ')',
                ];
            }
        } catch (\Exception $e) {
            $response['API_ERROR'] = [
                'comment' => $e->getMessage(),
            ];
        }
        return json_encode($response);
    }

    public function _getAttribute($attribute)
    {
        $model = ObjectManager::getInstance()->create('Magento\Catalog\Model\ResourceModel\Eav\Attribute');

        if (is_numeric($attribute)) {
            $model->load(int($attribute));
        } else {
            $model->load($attribute, 'attribute_code');
        }

        if (!$model->getId()) {
            $this->_fault('not_exists');
        }

        return $model;
    }

    public function _fault($phrase, $msg = null)
    {
        if (isset($msg)) {
            $phrase = $phrase . '(' . $msg . ')';
        }
        throw new \Exception($phrase);
    }

    /**
     * Decode date that receive from server
     *
     * @param $data
     */
    public function _dataDecode(&$data)
    {
        if (is_string($data)) {
            $data = json_decode($data, true);
        }
    }
}
